# Documentation sur Kubernetes
Source d'auteur: https://github.com/kekedaine/kubernetes-note
## Théorie

### [1. Vue d'ensemble et architecture de Kubernetes](/docs/1-introduction-k8s.md)
### [2. Concepts clés](/docs/2-term-k8s.md)
#### [2.1 Concept de Pod](/docs/2.1-pod-k8s.md)
#### [2.2 Concept de ReplicaSet](/docs/2.2-rs-k8s.md)
#### [2.3 Concept de Deployment](/docs/2.3-deployment-k8s.md)
#### [2.4 Concept de Scheduler](/docs/2.4-scheduler-k8s.md)
#### [2.5 Concept de Services](/docs/2.5-services-k8s.md)
#### [2.6 Concept de StatefulSets](/docs/2.6-sfs-k8s.md)
#### [2.7 Concept de DaemonSet](/docs/2.7-ds-k8s.md)
#### [2.8 Concept de Job](/docs/2.8-job-k8s.md)
#### [2.9 Concept d'Ingress](/docs/2.9-ingress-k8s.md)
#### 2.10 Concept de RBAC
### [3. Vue d'ensemble et architecture de Helm](/docs/3-helm-k8s.md)
### [4. Commandes couramment utilisées avec Kubectl](/docs/kubectl-cmd.md)
### 5. Vue d'ensemble de ETCD


## Pratique de base

### [Utilisation de Kubernetes 101](/docs/practise/kubernetes-101.md)
### [Lab Pratique avec Kubernetes 101](/src/practice/README.md)
### [1. Manipulation des Pods](/docs/practise/pod-101.md)
### [2. Manipulation des Deployments](/docs/practise/deployment-101.md)

## Pratique

### 1. Guide d'installation de Kubernetes (utilisation de Minikube)
### [2. Guide d'installation de Kubernetes (utilisation de Kubeadmin) sur CentOS 7](/docs/setup/install-k8s-centos7-kubeadm.md)
### [3. Déploiement de NFS comme stockage pour K8s](/docs/setup/install-nfs-storage-k8s.md)
### [4. Guide d'installation de Wordpress de base sur K8s](/docs/setup/setup-wordpress-basic.md)
### [5. Guide d'installation et utilisation de Helm](/docs/setup/install-helm-k8s.md)
### [6. Guide d'installation de Wordpress avec Helm](/docs/setup/install-wp-helm.md)
### [7. Guide d'installation du Dashboard Kubernetes](/docs/setup/setup-kubernetes-dashboard.md)
### [8. Guide d'installation de Weave Scope](/docs/setup/setup-weave-scope.md)
### [9. Guide de déploiement de MetalLB pour le cluster K8s](/docs/setup/install-metallb.md)
### [10. Guide de déploiement du Nginx Ingress Controller pour le cluster K8s](/docs/setup/install-nginx-ingress-helm.md)
### [11. Guide de déploiement du Prometheus Stack pour le cluster K8s](/docs/setup/install-prometheus-helm.md)
 
## Pratique avancée

### Déploiement d'ETCD séparé du Plan de contrôle
### Déploiement du Plan de contrôle HA K8s


## Problèmes

### [1. Problèmes lors du déploiement de Nginx Ingress pour exposer le service du cluster K8s](/docs/problem/setup-own-nginx-ingress-k8s.md)

## Références

### [1. Déploiement de Traefik comme Reverse Proxy pour Docker](/docs/setup/setup-traefik-docker.md)

