# Vue d'ensemble de Kubernetes

## Introduction

Kubernetes est une plateforme open source extensible, conçue pour gérer des applications empaquetées dans des conteneurs. Kubernetes est classé parmi les outils d'orchestration et fait partie de l'écosystème des conteneurs, également connu sous le nom de système d'orchestration de conteneurs. Avec Kubernetes (souvent abrégé en k8s), nous pouvons déployer, étendre et gérer facilement et automatiquement des applications basées sur des conteneurs. Les objets gérés par Kubernetes sont abstraits pour faciliter l'administration, et toutes les opérations de gestion s'effectuent via l'API Kubernetes.

Le nom "Kubernetes" provient du grec ancien et signifie "le pilote de navire" ou "le timonier". Google a ouvert la source de Kubernetes en 2014. Kubernetes repose sur plus d'une décennie d'expérience de Google dans l'exploitation d'une charge de travail importante en production, combinée aux meilleures pratiques et idées de la communauté.

Kubernetes peut être déployé sur une ou plusieurs machines physiques, des machines virtuelles ou une combinaison de machines physiques et virtuelles pour former un cluster Kubernetes.

## Architecture

L'architecture d'un cluster Kubernetes est relativement simple, les utilisateurs n'interagissent jamais directement avec les nœuds (Nodes) ; toutes les opérations de gestion se font via le "plan de contrôle" (control plane) à travers l'API.

L'architecture d'un cluster Kubernetes comprend deux composants principaux :
- Nœud (Worker Node)
- Plan de contrôle (Master Node)

![Schéma de l'architecture Kubernetes](/images/1-introduction-k8s/pic1.png)

### Nœud - Worker Node

Le nœud (Worker Node) joue le rôle de l'environnement d'exécution des conteneurs des applications utilisateur.

Il nécessite trois composants de base :
- `Container runtime` : L'environnement d'exécution des conteneurs, la technologie la plus couramment utilisée étant Docker.
- `kubelet` : Il reçoit des commandes du plan de contrôle (Master Node) pour créer, manipuler et gérer les conteneurs d'application en fonction des demandes de l'utilisateur.
- `kube-proxy` : Il permet aux utilisateurs d'accéder aux applications en cours d'exécution dans le cluster Kubernetes (dans un environnement de conteneur).

### Plan de contrôle - Master Node

Le plan de contrôle (Master Node) joue le rôle de composant de contrôle central de toutes les opérations et de la gestion des conteneurs sur les nœuds Workers.

Les principaux composants du nœud maître comprennent :
- `API-server (kube-apiserver)` :
  - Ce composant reçoit les demandes des utilisateurs ou d'autres applications.
  - Il est utilisé lorsque les utilisateurs ou d'autres applications souhaitent interagir avec le cluster Kubernetes.
  - Il fonctionne via une API REST.
  - Il écoute sur les ports 6443 (HTTPS) et 8080 (HTTP).
  - Il réside sur le nœud Maître.
- `Controller manager (kube-controller-manager)` :
  - Ce composant gère le cluster Kubernetes.
  - Il traite les demandes des utilisateurs ou d'autres applications, garantissant que les processus et les services s'exécutent correctement dans Kubernetes.
  - Il utilise le port 10252.
- `Schedule (kube-scheduler)` :
  - Ce composant attribue les Pods aux nœuds Workers.
  - Il utilise le port 10251.
- `Etcd` :
  - Il s'agit d'une base de données distribuée qui stocke les données sous forme de paires clé-valeur dans le cluster K8S.
  - Etcd est installé sur le nœud maître et stocke toutes les informations du cluster.
  - Il utilise le port 2380 pour écouter les demandes et le port 2379 pour que les clients envoient des demandes.

![Schéma de l'architecture Kubernetes](/images/1-introduction-k8s/pic2.png)

## Références

- [Documentation Kubernetes (en français)](https://kubernetes.io/fr/docs/concepts/overview/what-is-kubernetes/)
- [Introduction à Kubernetes](https://www.mirantis.com/blog/introduction-to-kubernetes/)
- [GitHub - Ghichep Kubernetes](https://github.com/hocchudong/ghichep-kubernetes/blob/master/docs/kubernetes-5min/03.Kientrucvacacthanhphan.md)
