# Termes de base de Kubernetes

Note :
- Dans cet article, nous aborderons les concepts de base de Kubernetes avant d'explorer des termes plus avancés.

## Kubectl
- Interface en ligne de commande pour administrer Kubernetes

![](/images/2-term-k8s/pic1.png)

## Nœud principal (Master Node)

- Composant responsable de la gestion des nœuds
- Reçoit et traite les demandes des utilisateurs et des administrateurs

![](/images/2-term-k8s/pic2.png)

## Nœud de travail (Worker Node)

- Géré par le nœud principal (Master Node)
- Reçoit des demandes du nœud principal (Master Node)
- Exécute des Pods (les conteneurs s'exécutent à l'intérieur des Pods)
- L'endroit où l'environnement des conteneurs s'exécute (comme Docker Engine), effectue des opérations de téléchargement d'images et exécute des conteneurs

![](/images/2-term-k8s/pic3.png)

## Kubelet

- Situé sur le nœud de travail (Worker Node)
- Reçoit des instructions du nœud principal (Master Node)
- Veille à ce que les conteneurs s'exécutent correctement sur le nœud de travail

![](/images/2-term-k8s/pic3.png)

## Pod Kubernetes

- Un Pod peut contenir un ou plusieurs conteneurs
- Un Pod est une entité appartenant aux Déploiements
- Chaque IP a une adresse IP différente à l'intérieur du cluster Kubernetes
- Le nombre de Pods peut être augmenté ou réduit de manière flexible (mise à l'échelle ou autoscaling des Pods)
- Les conteneurs appartenant à un Pod peuvent accéder à des volumes partagés
- Les données à l'intérieur d'un Pod sont perdues lorsque le Pod est arrêté (sauf si un stockage persistant est utilisé)

![](/images/2-term-k8s/pic4.png)

## Déploiement (Deployment)

- Étant donné que les Pods peuvent être créés et supprimés de manière flexible, un déploiement est nécessaire pour les gérer
- Le déploiement peut gérer les ressources allouées aux Pods, le nombre d'instances de Pods, et l'évolution du nombre d'instances de Pods selon les besoins
- Le déploiement se met automatiquement à jour lorsqu'un nouveau Pod est créé ou disparaît

![](/images/2-term-k8s/pic5.png)

## Secret

- Stocke et gère des informations sensibles telles que les identifiants de connexion du cluster Kubernetes
- Les objets Secrets sont mappés sur les Pods pour être utilisés (par exemple, les informations de connexion à la base de données)

## Service

- Responsable de la découverte et de la gestion des Pods dans le réseau interne de Kubernetes, ainsi que de l'exposition des services vers Internet
- Le Service identifie les Pods en fonction de la LabelSelector
- Il existe trois types de services :
  - ClusterIP :
    - IP interne de K8s représentant le groupe de Pods
    - Le trafic est équilibré entre les Pods du déploiement
  - Node Port :
    - Utilisé pour exposer une IP interne du cluster à l'extérieur
    - Mappe une paire "IP + Port interne" vers une paire "IP + Port externe" pour permettre l'accès aux services à l'intérieur du cluster depuis Internet
    - Le trafic est équilibré entre les Pods du déploiement
  - LoadBalancer :
    - Associe au déploiement une IP publique
    - Permet d'accéder au service via cette IP publique
    - Le trafic est équilibré entre les Pods à l'intérieur du cluster

## Source

https://collabnix.github.io/kubelabs/Kubernetes_Architecture.html

