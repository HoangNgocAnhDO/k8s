# Concept de Helm dans Kubernetes

## Vue d'ensemble

Helm est un gestionnaire de packages conçu spécifiquement pour Kubernetes. Tout comme les systèmes d'exploitation CentOS utilisent Yum ou Ubuntu utilise APT, Kubernetes a également besoin d'un outil de gestion des services s'exécutant sur le cluster Kubernetes. Avec Helm, la gestion des services devient plus simple par rapport à l'utilisation de kubectl pour définir ou déclarer des services. De plus, Helm permet le partage de ressources Helm, ce qui signifie qu'il existe des organisations fournissant des graphiques Helm (Helm Hub) prêts à être déployés pour accélérer la mise en place de services tels que WordPress, SELK, etc.

Utilisations courantes de Helm :
- Installation d'Apache, Nginx
- Installation de MariaDB, MySQL
- Installation de WordPress
- Installation de SELK
- etc.

## Concepts clés

- Chart : un ensemble de fichiers modèle YAML, ce sont des fichiers qui décrivent les ressources nécessaires pour initialiser une application. Par exemple, pour déployer WordPress sur Kubernetes, vous avez besoin d'images Docker telles que le code source, la base de données, etc. Helm organisera les images nécessaires pour créer l'application.
- Config : contenu du fichier values.yml, contenant des déclarations sur les images à utiliser, les variables d'environnement, les secrets nécessaires pour créer des services complets à partir du modèle Helm (par exemple : Ingress, Deployment, etc.).
- Release : une version spécifique d'une application Kubernetes en cours d'exécution basée sur un Chart et une configuration spécifique.
- Repositories : les Helm Charts peuvent être publiés via différents dépôts. Ils peuvent être des dépôts privés utilisés en interne dans une entreprise ou publics via Helm Hub. Certains Charts peuvent avoir plusieurs versions provenant de différentes entreprises ou éditeurs. Les Charts dans le dépôt Stable doivent toujours respecter les critères des exigences techniques de Helm.

## Architecture

Helm utilise une architecture client-serveur comprenant :
- Client CLI
- Serveur s'exécutant dans le cluster Kubernetes

Client Helm (CLI client) :
- Fournit une interface en ligne de commande permettant aux développeurs de gérer, initialiser, développer des Charts, des Configs, des Releases et des Repositories.
- Le client Helm interagit avec le serveur Tiller pour effectuer différentes actions telles que l'installation, la mise à jour et le retour en arrière avec les Charts et les Releases.

Serveur Tiller :
- Le serveur est situé dans le cluster Kubernetes et interagit avec le client Helm en communiquant avec le serveur API Kubernetes.
- Cela permet à Helm de gérer facilement les ressources Kubernetes avec des tâches telles que l'installation, la mise à jour, la requête et la suppression des ressources Kubernetes.

## Structure d'un package Helm

```
[root@master1181 apache]# tree
.
├── charts
│   └── common
│       ├── Chart.yaml
│       ├── README.md
│       ├── templates
│       │   ├── _affinities.tpl
│       │   ├── _capabilities.tpl
│       │   ├── _errors.tpl
│       │   ├── _images.tpl
│       │   ├── _labels.tpl
│       │   ├── _names.tpl
│       │   ├── _secrets.tpl
│       │   ├── _storage.tpl
│       │   ├── _tplvalues.tpl
│       │   ├── _utils.tpl
│       │   ├── _validations.tpl
│       │   └── _warnings.tpl
│       └── values.yaml
├── Chart.yaml
├── ci
│   └── ct-values.yaml
├── files
│   ├── README.md
│   └── vhosts
│       └── README.md
├── README.md
├── requirements.lock
├── requirements.yaml
├── templates
│   ├── configmap-vhosts.yaml
│   ├── configmap.yaml
│   ├── deployment.yaml
│   ├── _helpers.tpl
│   ├── ingress.yaml
│   ├── NOTES.txt
│   └── svc.yaml
├── values.schema.json
└── values.yaml

7 directories, 31 files
```

Rôles des fichiers et répertoires :
- `charts` : les charts dépendants peuvent être placés ici, mais il est toujours recommandé d'utiliser `requirements.yaml` pour lier les charts dépendants de manière plus flexible.
- `templates` : contient des fichiers de modèle pour générer des fichiers manifestes Kubernetes lorsqu'ils sont combinés avec les variables de configuration (à partir de `values.yaml` et de la ligne de commande). Notez que les fichiers de modèle utilisent la syntaxe du langage de programmation Go.
- `Chart.yaml` : fichier YAML contenant des informations sur la définition du Chart telles que le nom, la version, etc.
- `LICENSE/` : licence d'utilisation du Chart.
- `README.md` : description des informations et de l'utilisation du Chart, similaire à un README.md dans les projets sur GitHub.
- `requirements.yaml` : fichier YAML contenant la liste des liens vers les charts dépendants.
- `values.yaml` : fichier YAML contenant les valeurs de configuration par défaut pour le Chart.

## Sources

https://helm.sh/docs/

https://medium.com/@dugiahuy/kubernetes-helm-101-88074e2b76d9#:~:text=Helm%20est%20un%20gestionnaire%20de%20packages,l%C3%A0%20des%20ressources%20Kubernetes.

https://viblo.asia/p/helm-la-gi-no-co-lien-quan-gi-den-series-nay-Do754oAQlM6

