https://kubernetes.io/vi/docs/reference/kubectl/cheatsheet/#ng%E1%BB%AF-c%E1%BA%A3nh-v%C3%A0-c%E1%BA%A5u-h%C3%ACnh-kubectl

# Création d'objets Kubernetes

```sh
kubectl apply -f ./mon-manifeste.yaml              # Crée une ressource
kubectl apply -f ./mon1.yaml -f ./mon2.yaml        # Crée à partir de plusieurs fichiers
kubectl apply -f ./dossier                        # Crée des ressources à partir de tous les fichiers manifeste dans le dossier
kubectl apply -f https://git.io/vPieo             # Crée des ressources à partir d'une URL
kubectl create deployment nginx --image=nginx     # Crée un déploiement nginx
kubectl explain pods,svc                          # Obtenir des informations sur les manifestes des pods et des services

# Création de plusieurs objets YAML à partir de l'entrée standard
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: Pod
metadata:
  name: busybox-sleep
spec:
  containers:
  - name: busybox
    image: busybox
    args:
    - sleep
    - "1000000"
---
apiVersion: v1
kind: Pod
metadata:
  name: busybox-sleep-less
spec:
  containers:
  - name: busybox
    image: busybox
    args:
    - sleep
    - "1000"
EOF

# Création d'un secret avec plusieurs clés
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: Secret
metadata:
  name: monsecret
type: Opaque
data:
  password: $(echo -n "s33msi4" | base64 -w0)
  username: $(echo -n "jane" | base64 -w0)
EOF
```

## Visualiser et rechercher des ressources

```sh
# Commandes pour obtenir des sorties de base
kubectl get services                          # Liste tous les services dans l'espace de noms
kubectl get pods --all-namespaces             # Liste tous les pods dans tous les espaces de noms
kubectl get pods -o wide                      # Liste tous les pods dans l'espace de noms avec plus d'informations
kubectl get deployment mon-dep                # Liste un déploiement spécifique
kubectl get pods                              # Liste tous les pods dans l'espace de noms
kubectl get pod mon-pod -o yaml               # Obtient les informations d'un pod au format YAML
kubectl get pod mon-pod -o yaml --export      # Obtient les informations d'un pod au format YAML sans les détails du cluster

# Commandes describe
kubectl describe nodes mon-nœud
kubectl describe pods mon-pod

# Liste des services triés par nom
kubectl get services --sort-by=.metadata.name

# Liste des pods triés par nombre de redémarrages
kubectl get pods --sort-by='.status.containerStatuses[0].restartCount'

# Liste des pods triés par capacité de stockage dans l'espace de noms "test"
kubectl get pods -n test --sort-by='.spec.capacity.storage'

# Obtenir la version de tous les pods étiquetés avec "app=cassandra"
kubectl get pods --selector=app=cassandra -o \
  jsonpath='{.items[*].metadata.labels.version}'

# Liste de tous les nœuds worker (en utilisant un sélecteur pour exclure ceux étiquetés avec 'node-role.kubernetes.io/master')
kubectl get node --selector='!node-role.kubernetes.io/master'

# Liste de tous les pods en cours d'exécution dans l'espace de noms
kubectl get pods --field-selector=status.phase=Running

# Liste de toutes les ExternalIPs de tous les nœuds
kubectl get nodes -o jsonpath='{.items[*].status.addresses[?(@.type=="ExternalIP")].address}'

# Liste des noms des pods appartenant à un RC spécifique
# La commande "jq" est utile pour des transformations JSON plus complexes, consultez https://stedolan.github.io/jq/
sel=${$(kubectl get rc mon-rc --output=json | jq -j '.spec.selector | to_entries | .[] | "\(.key)=\(.value),"')%?}
echo $(kubectl get pods --selector=$sel --output=jsonpath={.items..metadata.name})

# Affichage des étiquettes de tous les pods (ou d'autres objets Kubernetes prenant en charge les étiquettes)
kubectl get pods --show-labels

# Vérifier quels nœuds sont prêts
JSONPATH='{range .items[*]}{@.metadata.name}:{range @.status.conditions[*]}{@.type}={@.status};{end}{end}' \
 && kubectl get nodes -o jsonpath="$JSONPATH" | grep "Ready=True"

# Liste de tous les Secrets actuellement utilisés par un pod
kubectl get pods -o json | jq '.items[].spec.containers[].env[]?.valueFrom.secretKeyRef.name' | grep -v null | sort | uniq

# Liste de tous les événements triés par horodatage de création
kubectl get events --sort-by=.metadata.creationTimestamp

```

## Mise à jour des ressources

```sh
kubectl set image deployment/frontend www=image:v2               # Met à jour le conteneur "www" du déploiement "frontend" en changeant l'image
kubectl rollout history deployment/frontend                      # Vérifie l'historique des déploiements, y compris les modifications
kubectl rollout undo deployment/frontend                         # Reviens à la version précédente du déploiement
kubectl rollout undo deployment/frontend --to-revision=2         # Reviens à une révision spécifique
kubectl rollout status -w deployment/frontend                    # Vérifie l'état du déploiement "frontend" en attendant qu'il soit terminé

# Plus utilisé depuis la version 1.11
kubectl rolling-update frontend-v1 -f frontend-v2.json           # (Obsolète) Met à jour les pods de frontend-v1
kubectl rolling-update frontend-v1 frontend-v2 --image=image:v2  # (Obsolète) Renomme la ressource et met à jour l'image
kubectl rolling-update frontend --image=image:v2                 # (Obsolète) Met à jour l'image du pod frontend
kubectl rolling-update frontend-v1 frontend-v2 --rollback        # (Obsolète) Annule le processus de mise à jour en cours

cat pod.json | kubectl replace -f -                              # Remplace un pod en utilisant un fichier JSON en entrée

# Remplace de force, supprime et recrée des ressources, provoquant une interruption de service
kubectl replace --force -f ./pod.json

# Crée un service pour nginx, exposé sur le port 80 et connecté aux conteneurs sur le port 8000
kubectl expose rc nginx --port=80 --target-port=8000

# Met à jour la version de l'image d'un seul conteneur vers v4
kubectl get pod mypod -o yaml | sed 's/\(image: myimage\):.*$/\1:v4/' | kubectl replace -f -

kubectl label pods my-pod new-label=awesome                      # Ajoute une étiquette
kubectl annotate pods my-pod icon-url=http://goo.gl/XXBTWq       # Ajoute une annotation
kubectl autoscale deployment foo --min=2 --max=10                # Met en échelle automatiquement le déploiement "foo"
```

## Gérer les ressources

```sh
# Mettre à jour une partie d'un nœud
kubectl patch node k8s-node-1 -p '{"spec":{"unschedulable":true}}'

# Mettre à jour l'image d'un conteneur ; spec.containers[*].name est obligatoire car c'est la clé de mise à jour
kubectl patch pod pod-valide -p '{"spec":{"containers":[{"name":"kubernetes-serve-hostname","image":"nouvelle image"}]}}'

# Mettre à jour l'image d'un conteneur en utilisant un patch JSON avec des chemins d'accès
kubectl patch pod pod-valide --type='json' -p='[{"op": "replace", "path": "/spec/containers/0/image", "value":"nouvelle image"}]'

# Désactiver une livenessProbe d'un déploiement en utilisant un patch JSON avec des chemins d'accès
kubectl patch deployment déploiement-valide --type json -p='[{"op": "remove", "path": "/spec/template/spec/containers/0/livenessProbe"}]'

# Ajouter un nouvel élément à un tableau d'endroits
kubectl patch sa default --type='json' -p='[{"op": "add", "path": "/secrets/1", "value": {"name": "quelquechose" } }]'
```

## Modification des ressources

## Mise à l'échelle des ressources

```sh
kubectl scale --replicas=3 rs/foo                                 # Mettre à l'échelle un replicaset nommé 'foo' à 3
kubectl scale --replicas=3 -f foo.yaml                            # Mettre à l'échelle une ressource définie dans "foo.yaml" à 3
kubectl scale --current-replicas=2 --replicas=3 deployment/mysql  # Si la taille actuelle du déploiement mysql est de 2, mettre à l'échelle mysql à 3
kubectl scale --replicas=5 rc/foo rc/bar rc/baz                   # Mettre à l'échelle plusieurs contrôleurs de réplication
```

## Suppression des ressources

```sh
kubectl delete -f ./pod.json                                              # Supprimer un pod en utilisant le type et le nom définis dans pod.json
kubectl delete pod,service baz foo                                        # Supprimer les pods et services nommés "baz" et "foo"
kubectl delete pods,services -l name=myLabel                              # Supprimer les pods et services avec l'étiquette name=myLabel
kubectl -n my-ns delete pod,svc --all                                     # Supprimer tous les pods et services dans l'espace de noms my-ns

# Supprimer tous les pods correspondant à pattern1 ou pattern2
kubectl get pods  -n mynamespace --no-headers=true | awk '/pattern1|pattern2/{print $1}' | xargs  kubectl delete -n mynamespace pod
```

## Interaction avec les pods en cours d'exécution

```sh
kubectl logs my-pod                                 # Afficher les journaux du pod (stdout)
kubectl logs -l name=myLabel                        # Afficher les journaux des pods avec l'étiquette name=myLabel (stdout)
kubectl logs my-pod --previous                      # Afficher les journaux du pod (stdout) pour le conteneur précédent
kubectl logs my-pod -c my-container                 # Afficher les journaux du conteneur du pod (stdout, en cas de plusieurs conteneurs)
kubectl logs -l name=myLabel -c my-container        # Afficher les journaux du conteneur nommé my-container avec l'étiquette name=myLabel (stdout)
kubectl logs my-pod -c my-container --previous      # Afficher les journaux du conteneur my-container du pod my-pod (stdout, en cas de plusieurs conteneurs) pour le conteneur précédent
kubectl logs -f my-pod                              # Suivre les journaux du pod my-pod (stdout)
kubectl logs -f my-pod -c my-container              # Suivre les journaux du conteneur my-container dans le pod my-pod (stdout, en cas de plusieurs conteneurs)
kubectl logs -f -l name=myLabel --all-containers    # Suivre les journaux de tous les conteneurs du pod avec l'étiquette name=myLabel (stdout)
kubectl run -i --tty busybox --image=busybox -- sh  # Exécuter un pod dans un shell interactif
kubectl run nginx --image=nginx --restart=Never -n 
mynamespace                                         # Exécuter un pod nginx dans un espace de noms spécifique
kubectl run nginx --image=nginx --restart=Never     # Exécuter un pod nginx et enregistrer sa spécification dans un fichier nommé pod.yaml
--dry-run -o yaml > pod.yaml

kubectl attach my-pod -i                            # Attacher au conteneur en cours d'exécution
kubectl port-forward my-pod 5000:6000               # Faire une écoute sur le port 5000 de la machine locale et rediriger vers le port 6000 du pod my-pod
kubectl exec my-pod -- ls /                         # Exécuter une commande dans un pod (pour un conteneur unique)
kubectl exec my-pod -c my-container -- ls /         # Exécuter une commande dans un pod (en cas de plusieurs conteneurs)
kubectl top pod NOM_DU_POD --containers               # Afficher les statistiques du pod et de ses conteneurs en cours d'exécution
```

## Interaction avec les nœuds et le cluster

```sh
kubectl cordon my-node                                                # Marquer my-node comme non planifiable
kubectl drain my-node                                                 # Évacuer my-node du cluster en préparation pour la maintenance
kubectl uncordon my-node                                              # Marquer my-node comme planifiable à nouveau
kubectl top node my-node                                              # Afficher les statistiques du nœud
kubectl cluster-info                                                  # Afficher l'adresse du maître et les services du cluster
kubectl cluster-info dump                                             # Exporter l'état actuel du cluster vers la sortie standard
kubectl cluster-info dump --output-directory=/path/to/cluster-state   # Exporter l'état actuel du cluster vers /path/to/cluster-state

kubectl taint nodes foo dedicated=special-user:NoSchedule
```
