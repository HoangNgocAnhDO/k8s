# Opérations avec les Deployments Kubernetes (K8s)

## Création d'un nouveau Deployment

```bash
kubectl create deployments nginx --image=nginx
```

Résultat :
```bash
[root@master1181 ~]# kubectl create deployments nginx --image=nginx 
deployment.apps/nginx créé

[root@master1181 ~]# kubectl get pods 
NOM                      PRÊT   STATUT               REDEMARRES   ÂGE
nginx-6799fc88d8-7rvzp   0/1     En création de conteneur  0          6s

[root@master1181 ~]# kubectl get pods 
NOM                      PRÊT   STATUT    REDEMARRES   ÂGE
nginx-6799fc88d8-7rvzp   1/1     En cours d'exécution  0          108s
```

## Obtenir la liste des Deployments

```bash
kubectl get deployments
```

Résultat :
```bash
[root@master1181 ~]# kubectl get deployments
NOM    PRÊT   À JOUR   DISPONIBLE   ÂGE
nginx   1/1     1            1           3m31s
```

## Afficher la description du Deployment

```bash
kubectl describe deployments nginx
```

Résultat :
```bash
[root@master1181 ~]# kubectl describe deployments nginx
Nom:                   nginx
Espace de noms:        par défaut
Date de création:      Ven, 30 Oct 2020 16:44:32 +0700
Étiquettes:            app=nginx
Annotations:           deployment.kubernetes.io/revision: 1
Sélecteur:             app=nginx
Réplicas:              1 souhaité | 1 mis à jour | 1 total | 1 disponible | 0 indisponible
Type de Stratégie:     Mise à jour progressive
Secondes Min. Prêtes:  0
Stratégie de Mise à Jour Progressive:  25% max indisponible, 25% max en plus
Modèle de Pod:
  Étiquettes:  app=nginx
  Conteneurs:
   nginx:
    Image:        nginx
    Port:         <aucun>
    Port hôte:    <aucun>
    Environnement:  <aucun>
    Montages:       <aucun>
  Volumes:        <aucun>
Conditions:
  Type           Statut  Raison
  ----           ------  ------
  Disponible      Vrai    Nombre minimum de réplicas disponibles
  En progression  Vrai    Nouveau ReplicaSet disponible
Anciens ReplicaSets:  <aucun>
Nouveau ReplicaSet:   nginx-6799fc88d8 (1/1 réplicas créés)
Événements:
  Type    Raison             Âge    De                   Message
  ----    ------             ----   ----                 -------
  Normal  ScalingReplicaSet  4m28s  deployment-controller  Replica set nginx-6799fc88d8 monté à 1
```

## Suppression du Deployment

```bash
kubectl delete deployments nginx
```

Résultat :
```bash
[root@master1181 ~]# kubectl delete deployments nginx
deployment.apps "nginx" supprimé
```

