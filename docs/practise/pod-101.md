# Opérations avec les Pods Kubernetes (K8s)

## Création d'un Pod

```bash
kubectl run nginx --restart=Never --image=nginx 
```

Résultat :
```bash
[root@master1181 ~]# kubectl run nginx --restart=Never --image=nginx 
pod/nginx créé
```

## Obtenir la liste des Pods

```bash
kubectl get pods -o wide
```

Résultat :
```bash
[root@master1181 ~]# kubectl get pods -o wide
NOM    PRÊT   STATUT    REDEMARRES   ÂGE   IP             NŒUD        NŒUD NOMINÉ   PORTES DE LIRETÉ
nginx   1/1     En cours d'exécution   0          85s   10.244.1.103   worker1182   <aucun>           <aucun>
```

## Afficher la description du Pod

```bash
kubectl describe pods nginx
```

Résultat :
```bash
[root@master1181 ~]# kubectl describe pods nginx
Nom:         nginx
Espace de noms:    par défaut
Priorité:     0
Nœud:         worker1182/10.10.12.82
Heure de début:   Ven, 30 Oct 2020 16:33:21 +0700
Étiquettes:       run=nginx
Annotations:  <aucune>
Statut:       En cours d'exécution
IP:           10.244.1.103
IPs:
  IP:  10.244.1.103
Conteneurs:
  nginx:
    ID du conteneur:   docker://52a41ed38ad61b9f8d2a3d6c6ad918da99ca54ff60bd662c0c2bbfbaf51f229d
    Image:          nginx
    ID de l'image:       docker-pullable://nginx@sha256:ed7f815851b5299f616220a63edac69a4cc200e7f536a56e421988da82e44ed8
    Port:           <aucun>
    Port hôte:      <aucun>
    État:          En cours d'exécution
      Démarré:      Ven, 30 Oct 2020 16:33:28 +0700
    Prêt:          Vrai
    Nombre de redémarrages:  0
    Environnement:    <aucun>
    Montages:
      /var/run/secrets/kubernetes.io/serviceaccount depuis default-token-4cpsj (ro)
Conditions:
  Type              Statut
  Initialisé       Vrai 
  Prêt             Vrai 
  ConteneursPrêts   Vrai 
  PodProgrammé      Vrai 
Volumes:
  default-token-4cpsj:
    Type:        Secret (un volume rempli par un Secret)
    NomSecret:  default-token-4cpsj
    Optionnel:    faux
Classe de QoS:       Meilleur effort
Sélecteurs de nœud:  <aucun>
Tolérances:     node.kubernetes.io/not-ready:NoExecute op=Exists pendant 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists pendant 300s
Événements:
  Type    Raison     Âge    De               Message
  ----    ------     ----   ----             -------
  Normal  Programmé  7m34s  planificateur par défaut  Attribution réussie de default/nginx à worker1182
  Normal  Tirage     7m33s  kubelet            Tirage de l'image "nginx"
  Normal  Tiré     7m27s  kubelet            Image "nginx" tirée avec succès en 6.074740651s
  Normal  Créé    7m27s  kubelet            Conteneur nginx créé
  Normal  Démarré    7m27s  kubelet            Conteneur nginx démarré
```

## Suppression du Pod

```bash
kubectl delete pods nginx
```

Résultat :
```bash
[root@master1181 ~]# kubectl delete pods nginx
pod "nginx" supprimé

[root@master1181 ~]# kubectl get pods -o wide
Aucune ressource trouvée dans l'espace de noms par défaut.
```

