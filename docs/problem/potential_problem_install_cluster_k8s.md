# Problèmes lors d'installation du cluster K8s

## Problème 1

Les problèmes potentiels sont avec kube-proxy et coredns

![](/images/problem/installation-cluster-k8s/pic1.jpg)
## Solution 
Effectuer d'abord reset pour réinitialiser l'environnement
```
sudo kubeadm reset
```
Effectuer la commande init en éliminant ces packages

```
sudo kubeadm init --apiserver-advertise-address 192.168.22.2 --pod-network-cidr=10.244.0.0/16 --skip-phases addon/coredns,addon/kube-proxy
```
Installer ces packages juste après la commande init

```
kubeadm init --kubernetes-version=v${K8S_VERSION} phase addon kube-proxy
kubeadm init --kubernetes-version=v${K8S_VERSION} phase addon coredns
```

## Problème 2 

Manque du client nerdctl pour containerd afin d'utiliser kubernetes

## Solution
Télécharger nerdctl :

```
sudo wget https://github.com/containerd/nerdctl/releases/download/v0.11.0/nerdctl-0.11.0-linux-amd64.tar.gz
```
Extraire et déplacer nerdctl dans un répertoire système :

```
sudo tar Cxzvvf /usr/local/bin nerdctl-0.11.0-linux-amd64.tar.gz
```
Vérifier l'installation de nerdctl :

```
nerdctl version
```
## Problème 3

```
[cloudadm@onerec-master-1 ~]$ kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
The connection to the server 192.168.22.2:6443 was refused - did you specify the right host or port?
```
## Solution
Vérification et Configuration pour Kubernetes et Docker

Désactiver le Swap

Premièrement, vérifiez si le swap est désactivé sur votre nœud car vous DEVEZ désactiver le swap pour que le kubelet fonctionne correctement.

```bash
sudo swapoff -a  
sudo sed -i '/ swap / s/^/#/' /etc/fstab
```
Vérifier le Pilote de Groupe de Contrôle (Cgroup) pour Kubernetes et Docker

Vérifiez également si le pilote de groupe de contrôle (cgroup) de Kubernetes et de Docker est défini sur le même. Selon la documentation de Kubernetes :

    Les deux, le runtime de conteneur et le kubelet, ont une propriété appelée "pilote de cgroup", qui est importante pour la gestion des cgroups sur les machines Linux.

    Avertissement : Il est nécessaire d'harmoniser le pilote de cgroup du runtime de conteneur et celui du kubelet, sinon le processus kubelet échouera.

La page des Runtimes de Conteneurs explique que le pilote systemd est recommandé pour les configurations basées sur kubeadm au lieu du pilote cgroupfs, car kubeadm gère le kubelet en tant que service systemd.

Pour Docker :
```
docker info |grep -i cgroup
```
Vous pouvez ajouter ceci à /etc/docker/daemon.json pour définir le pilote de cgroup de Docker sur systemd :
```
{
    "exec-opts": ["native.cgroupdriver=systemd"]
}
```
Redémarrez votre service Docker après avoir effectué des modifications avec :
```
sudo systemctl daemon-reload
sudo systemctl restart docker
sudo systemctl restart kubelet
```

