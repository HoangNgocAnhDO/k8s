# Problèmes lors du déploiement de Nginx Ingress pour exposer le service du cluster K8s

## Problème

Dans un environnement de fournisseur de cloud, le composant Load Balancer est déjà disponible. Lorsqu'il est combiné avec un Ingress Controller tel que Nginx Ingress, exposer des services devient très simple. Mais comment exposer des services si on déploie soi-même un cluster K8s?

![](/images/problem/setup-own-nginx-ingress-k8s/pic1.jpg)

![](/images/problem/setup-own-nginx-ingress-k8s/pic2.jpg)

## Solution 1 : Déployer MetalLB

MetalLB fournit un load-balancer réseau pour les clusters K8s auto-déployés, en dehors des fournisseurs de cloud. MetalLB peut être déployé facilement via un fichier manifeste ou via Helm.

Une fois MetalLB déployé, les services Nginx Ingress se verront attribuer une `IP Externe` par MetalLB. Cette IP peut alors être utilisée pour exposer les services du cluster à Internet.

Problème :
- MetalLB est actuellement en version bêta (expérimentale).

Pour en savoir plus :
- [Guide de déploiement de MetalLB pour les clusters K8s](/docs/setup/install-metallb.md)
- [Guide de déploiement du Nginx Ingress Controller pour les clusters K8s](/docs/setup/install-nginx-ingress-helm.md)

La figure ci-dessous illustre l'utilisation de L2 MetalLB en combinaison avec Nginx Ingress Controller pour exposer un service à l'extérieur.

![](/images/problem/setup-own-nginx-ingress-k8s/pic3.jpg)

## Solution 2 : Déployer Nginx Ingress Controller via un service NodePort

Remarque :
- Les services de type NodePort peuvent utiliser uniquement les ports de 30000 à 32767 sur tous les nœuds du cluster Kubernetes.

![](/images/problem/setup-own-nginx-ingress-k8s/pic4.jpg)

## Solution 3 : Déployer Nginx Ingress Controller en utilisant le réseau hôte

À utiliser lorsqu'il n'y a pas de Load Balancer externe et que la méthode NodePort n'est pas une option. Les Pods Ingress-Nginx peuvent être configurés pour utiliser le réseau hôte (c'est-à-dire l'IP et le port physique de la VM ou du serveur). Cette méthode permet à Nginx Ingress Controller de lier les ports 80 et 443.

Le principal inconvénient est qu'il ne peut y avoir qu'un seul Nginx Ingress par Worker node. Avec cette solution, Nginx Ingress Controller est déployé via DaemonSets.

Autres inconvénients :
- Impossible d'utiliser le DNS interne du cluster Kubernetes.
- Impossible de vérifier l'état de l'Ingress.

Configuration dans Pod Spec
\```yaml
template:
  spec:
    hostNetwork: true
\```

![](/images/problem/setup-own-nginx-ingress-k8s/pic5.jpg)

## Solution 4 : Déployer un composant Expose distinct

Cette méthode combine NodePort avec des solutions logicielles comme Reverse Proxy, Load Balancer tels que HAProxy, Nginx, ou utilise des équipements matériels. 
Le réseau Edge Node reçoit une IP publique et tout le trafic passe par ce composant pour atteindre le cluster. Le trafic arrive d'abord aux ports 80 ou 443, puis est transmis par le réseau Edge Node aux NodePorts correspondants.

![](/images/problem/setup-own-nginx-ingress-k8s/pic6.jpg)

## Source

https://kubernetes.github.io/ingress-nginx/deploy/baremetal/

