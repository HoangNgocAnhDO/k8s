# Architecture et composants de K8S

Kubernetes peut être déployé sur une ou plusieurs machines physiques, machines virtuelles, ou une combinaison des deux pour former un cluster. Ce cluster, contrôlé par Kubernetes, crée des conteneurs en réponse aux demandes des utilisateurs. L'architecture logique de Kubernetes comprend deux composants principaux basés sur le rôle des nœuds : le nœud Master et le nœud Worker.

## Nœud Master

Le nœud Master agit comme le Control plane, contrôlant toutes les opérations générales et gérant les conteneurs sur les nœuds Worker. Les principaux composants du nœud Master incluent : l'API-server, le Controller-manager, le Scheduler, Etcd et même le Docker Engine. Note : Bien que Docker ne soit pas visible dans certains schémas, il est nécessaire sur le nœud Master pour exécuter les composants de K8S dans des conteneurs.

## Nœud Worker

Le rôle principal du nœud Worker est de fournir un environnement pour exécuter les conteneurs demandés par les utilisateurs. Par conséquent, les composants principaux du nœud Worker comprennent : kubelet, kube-proxy, et bien sûr Docker.

## Composants principaux dans un cluster K8S

- etcd
- API-server
- Controller-manager
- Scheduler
- Agent
- Proxy
- CLI

### Container runtime

C'est le composant qui exécute les applications sous forme de conteneurs. Généralement, Docker est utilisé à cet effet.

#### etcd

Etcd est une base de données distribuée utilisant une écriture clé/valeur au sein du cluster K8S. Installé sur le nœud Master, etcd stocke toutes les informations du Cluster. Il utilise le port 2380 pour l'écoute des requêtes et le port 2379 pour les requêtes clients.

#### API-server

L'API server reçoit les requêtes du système K8S via REST, c'est-à-dire qu'il reçoit des directives des utilisateurs aux services du cluster via l'API, ce qui signifie que les utilisateurs ou d'autres services du cluster peuvent interagir avec K8S via HTTP/HTTPS. L'API-server fonctionne sur les ports 6443 (HTTPS) et 8080 (HTTP).

#### Controller-manager

Le Controller-manager est un composant de gestion dans K8S, responsable du traitement des tâches dans le cluster pour assurer le fonctionnement des ressources. Ses composants internes incluent :

- Node Controller : Reçoit et répond aux notifications lorsqu'un nœud est en panne.
- Replication Controller : Assure le maintien du nombre exact de répliques et la distribution des conteneurs dans les pods.
- Endpoints Controller : Remplit l'objet Endpoints (c'est-à-dire, joint les services et les pods).
- Service Account & Token Controllers : Crée des comptes et des jetons pour l'utilisation des API dans les namespaces.

Le Controller-manager fonctionne sur le nœud Master et utilise le port 10252.

#### Scheduler

Le kube-scheduler est chargé de surveiller et de sélectionner des nœuds pour les pods nouvellement demandés. Il choisit les nœuds les plus appropriés en fonction de ses algorithmes de planification. Le kube-scheduler est installé sur le nœud Master et utilise le port 10251.

#### Agent - kubelet

L'agent, ou kubelet, est un composant clé qui fonctionne sur les nœuds Worker. Lorsque le kube-scheduler a identifié un nœud pour un pod, il envoie les informations de configuration (images, volumes...) au kubelet sur ce nœud. Le kubelet crée ensuite les conteneurs selon ces spécifications.

Les fonctions principales de kubelet sont :

- Surveiller les pods affectés à son nœud.
- Monter les volumes pour le pod.
- Assurer le bon fonctionnement des conteneurs du pod sur ce nœud (le nœud Worker ayant Docker installé).
- Signaler l'état des pods au cluster pour savoir si les conteneurs fonctionnent correctement.

Kubelet fonctionne sur les nœuds Worker et utilise les ports 10250 et 10255.

#### Proxy

Les services fonctionnent seulement de manière logique, donc pour que l'extérieur puisse accéder à ces services, il est nécessaire d'avoir un composant qui redirige les requêtes de l'extérieur vers l'intérieur. Kube-proxy est installé sur les nœuds Worker et utilise le port 31080.

#### CLI

kubectl est un outil en ligne de commande permettant aux utilisateurs d'interagir avec K8S. kubectl peut être exécuté sur n'importe quelle machine, tant qu'elle peut se connecter à l'API-server K8S.

### Pod network

Comme mentionné dans les sections précédentes, le réseau des pods est un autre composant gérant le réseau dans le cluster K8S. Il assure la communication entre les conteneurs. Il existe de nombreuses options pour le réseau des pods, mais ce document ne présente que Flannel.

## Master node

Le Master node est le serveur qui contrôle les Worker Nodes exécutant les applications. Il comprend quatre composants principaux :
- Kubernetes API Server : permet aux différents composants de communiquer entre eux.
- Scheduler : planifie le déploiement des applications sur les Workers.
- Controler Manager : gère les Workers, vérifie s'ils sont opérationnels ou non, et gère la réplication des applications.
- Etcd : base de données de Kubernetes, stockant toutes ses informations.

## Worker node

Le Worker node est le serveur qui exécute les applications. Il se compose de trois composants principaux :
- Container runtime : exécute les applications sous forme de conteneurs, généralement Docker.
- Kubelet : communique avec l'API Server de Kubernetes et gère les conteneurs.
- Kubernetes Service Proxy : gère la répartition de charge entre les applications.

## Pod

Le Pod est le concept le plus fondamental et important de Kubernetes. Un Pod peut contenir un ou plusieurs conteneurs. C'est l'endroit où les applications sont exécutées. Les Pods sont des processus sur les Worker Nodes, ayant leurs propres ressources en termes de système de fichiers, CPU, RAM, volumes, adresse réseau...

## Image

C'est le logiciel d'application emballé pour s'exécuter en tant que conteneur. Les Pods utilisent ces Images pour fonctionner. Ces Images sont généralement gérées dans un stockage centralisé, comme Docker Hub, qui contient les Images d'applications populaires telles que nginx, mysql, wordpress...

## Deployment

C'est la méthode utilisée pour déployer, mettre à jour et gérer les Pods.

## Replicas Controller

C'est le composant qui gère les copies des Pods, aidant à répliquer ou réduire le nombre de Pods.

## Service

C'est la partie réseau de Kubernetes qui stabilise les appels entre les Pods, ou pour équilibrer la charge entre plusieurs copies de Pod, et peut être utilisé pour diriger le trafic des utilisateurs vers l'application (Pod), permettant aux utilisateurs d'accéder à l'application.

## Label

Les Labels sont utilisés pour classer et gérer les Pods. Par exemple, nous pouvons étiqueter les Pods selon leur fonctionnalité (frontend, backend), ou selon l'environnement de fonctionnement (dev, qc, uat, production)...

Ces concepts sont les plus fondamentaux que nous souhaitons introduire. Kubernetes comporte de nombreux autres concepts, que nous aborderons progressivement.

https://medium.com/vinid/gi%E1%BB%9Bi-thi%E1%BB%87u-v%E1%BB%81-kubernetes-kh%C3%A1i-ni%E1%BB%87m-c%C6%A1-b%E1%BA%A3n-v%C3%A0-th%E1%BB%B1c-h%C3%A0nh-ngay-tr%C3%AAn-tr%C3%ACnh-duy%E1%BB%87t-web-8fbea30053e7

### Mise en réseau des applications avec Service pour l'accès utilisateur

Les Pods sont susceptibles d'être arrêtés ou redémarrés pour diverses raisons, ce qui entraîne des changements fréquents dans leurs adresses IP. Si les applications se connectent entre elles via l'IP du Pod, cela manque de stabilité. C'est pourquoi la fonctionnalité Service de Kubernetes a été créée, permettant de représenter le Pod par un Service ayant une IP fixe, se connectant au Pod. De plus, le Service sert également de Load Balancer pour les applications ayant plusieurs répliques et de nombreux Pods. Pour connecter le Service au Pod, Kubernetes utilise des concepts supplémentaires tels que les Labels et les Selectors. Un Label est une manière de nommer une application pour faciliter son administration et sa programmation dans un environnement où des centaines, voire des milliers d'applications, sont exécutées sur Kubernetes. Un Selector est un mot-clé utilisé pour relier le Service aux Labels des Pods.

[Plus d'informations sur l'architecture et les contrôleurs cloud de Kubernetes](https://kubernetes.io/vi/docs/concepts/architecture/cloud-controller/)


