# Déploiement de WordPress selon la documentation officielle

# https://kubernetes.io/docs/tutorials/stateful-application/mysql-wordpress-persistent-volume/

## Préparation des nœuds pour WordPress
```bash
[root@nfs1099 nfsshare]# ll
drwxrwxrwx 2 nfsnobody nfsnobody  6 16:07 23 Oct wordpress-data-1
drwxrwxrwx 2 nfsnobody nfsnobody  6 16:07 23 Oct wordpress-data-2
```

## Sur le master Kubernetes

```bash
mkdir -p /root/wp-k8s
cd /root/wp-k8s
```

```bash
[root@master1181 wp-k8s]# cat wordpress-pv.yml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: wordpress-data-1
spec:
  capacity:
    storage: 20Gi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteOnce
  nfs:
    path: /var/nfsshare/wordpress-data-1
    server: 10.10.11.99
    readOnly: false
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: wordpress-data-2
spec:
  capacity:
    storage: 20Gi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteOnce
  nfs:
    path: /var/nfsshare/wordpress-data-2
    server: 10.10.11.99
    readOnly: false

[root@master1181 wp-k8s]# kubectl apply -f wordpress-pv.yml
persistentvolume/wordpress-data-1 créé
persistentvolume/wordpress-data-2 créé

[root@master1181 wp-k8s]# kubectl get pv
NOM               CAPACITÉ   MODES D'ACCÈS   POLITIQUE DE RÉCUPÉRATION   STATUT      REVENDICATION   CLASSE DE STOCKAGE   RAISON   ÂGE
wordpress-data-1   20Gi       RWO            Conserver                   Disponible                                   39s
wordpress-data-2   20Gi       RWO            Conserver                   Disponible                                   39s
```

```bash
[root@master1181 wp-k8s]# curl -LO https://k8s.io/examples/application/wordpress/mysql-deployment.yaml
[root@master1181 wp-k8s]# curl -LO https://k8s.io/examples/application/wordpress/wordpress-deployment.yaml

[root@master1181 wp-k8s]# echo -n 'admin' | base64
YWRtaW4=

[root@master1181 wp-k8s]# cat wordpress-secret.yml
apiVersion: v1
kind: Secret
metadata:
  name: mysql-pass
type: Opaque
data:
  password: YWRtaW4=

[root@master1181 wp-k8s]# kubectl apply -f wordpress-secret.yml
secret/mysql-pass créé

[root@master1181 wp-k8s]# kubectl get secret 
NOM                                   TYPE                                  DONNÉES   ÂGE
default-token-4cpsj                    kubernetes.io/service-account-token   3      7d21h
kube-prometheus-stack-1603-admission   Opaque                                3      6h37m
mysecret                               Opaque                                2      3d
mysql-pass                             Opaque                                1      12s
prometheus-operator-160335-admission   Opaque                                3      24h

[root@master1181 wp-k8s]# kubectl describe secret mysql-pass
Nom:         mysql-pass
Espace de noms:    par défaut
Étiquettes:       <aucune>
Annotations:  <aucune>

Type:  Opaque

Données
====
password:  5 octets

[root@master1181 wp-k8s]# ls -alh
total 24K
drwxr-xr-x   2 root root  140 16:26 23 Oct .
dr-xr-x---. 12 root root 4,0K 16:04 23 Oct ..
-rw-r--r--   1 root root 1,3K 16:04 23 Oct mysql-deployment.yaml
-rw-r--r--   1 root root    5 16:18 23 Oct password.txt
-rw-r--r--   1 root root 1,3K 16:04 23 Oct wordpress-deployment.yaml
-rw-r--r--   1 root root  526 16:11 23 Oct wordpress-pv.yml
-rw-r--r--   1 root root   98 16:26 23 Oct wordpress-secret.yml

[root@master1181 wp-k8s]# kubectl apply -f mysql-deployment.yaml
service/wordpress-mysql créé
persistentvolumeclaim/mysql-pv-claim créé
deployment.apps/wordpress-mysql créé

[root@master1181 wp-k8s]# kubectl get pods
NOM                              PRÊT   STATUT    REDEMARRES   ÂGE
wordpress-mysql-6c479567b-4hx7v   1/1     En cours d'exécution   0          8s

[root@master1181 wp-k8s]# kubectl apply -f wordpress-deployment.yaml
service/wordpress créé
persistentvolumeclaim/wp-pv-claim créé
deployment.apps/wordpress créé

[root@master1181 wp-k8s]# kubectl get pvc
NOM             STATUT   VOLUME             CAPACITÉ   MODES D'ACCÈS   CLASSE DE STOCKAGE   ÂGE
mysql-pv-claim   Lié    wordpress-data-1   20Gi       RWO                           91s
wp-pv-claim      Lié    wordpress-data-2   20Gi       RWO                           45s

[root@master1181 wp-k8s]# kubectl get pods
NOM                              PRÊT   STATUT    REDEMARRES   ÂGE
wordpress-5994d99f46-fvctt        1/1     En cours d'exécution   0          53s
wordpress-mysql-6c479567b-4hx7v   1/1     En cours d'exécution   0          99s

[root@master1181 wp-k8s]# kubectl get services
NOM              TYPE           IP DE CLUSTER       IP EXTERNE   PORT(S)        ÂGE
kubernetes        ClusterIP      10.96.0.1        <aucune>        443/TCP        7d21h
wordpress         LoadBalancer   10.102.188.195   <en attente>     80:32030/TCP   2m33s
wordpress-mysql   ClusterIP      Aucune             <aucune>        3306/TCP       3m19s


[root@master1181 wp-k8s]# kubectl edit services wordpress
spec:
  clusterIP: 10.102.188.195
  externalTrafficPolicy: Cluster
  ports:
  - nodePort: 32030
    port: 80
    protocole: TCP
    targetPort: 80
  selector:
    app: wordpress
    tier: frontend
  sessionAffinity: Aucune
  type: NodePort

[root@master1181 wp-k8s]# kubectl get services
NOM              TYPE        IP DE CLUSTER       IP EXTERNE   PORT(S)        ÂGE
kubernetes        ClusterIP   10.96.0.1        <aucune>        443/TCP        7d21h
wordpress         NodePort    10.102.188.195   <aucune>        80:32030/TCP   4m32s
wordpress-mysql   ClusterIP   Aucune             <aucune>        3306/TCP       5m18s

# Accès : http://10.10.11.81:32030/wp-admin/install.php
# Configuration et Connexion
```

