https://medium.com/vinid/gi%E1%BB%9Bi-thi%E1%BB%87u-v%E1%BB%81-kubernetes-kh%C3%A1i-ni%E1%BB%87m-c%C6%A1-b%E1%BA%A3n-v%C3%A0-th%E1%BB%B1c-h%C3%A0nh-ngay-tr%C3%AAn-tr%C3%ACnh-duy%E1%BB%87t-web-8fbea30053e7

# Gestion des pods dans Kubernetes

Pour lister les pods, nous utilisons la commande :
```bash
kubectl get pods
```

Pour obtenir des détails plus précis sur les pods en cours d'exécution, tels que l'image utilisée par le pod, les variables d'environnement, les ressources déclarées, etc., nous utilisons la commande :
```bash
kubectl describe pods
```

Pour voir les logs d'une application en cours d'exécution, nous utilisons la commande :
```bash
kubectl logs $POD_NAME
```
où $POD_NAME est le nom du pod de l'application dont vous voulez voir les logs.

Pour exécuter une commande à l'intérieur de l'application, nous utilisons la commande :
```bash
kubectl exec $POD_NAME env
```

Pour initialiser un bash shell dans un pod de l'application, nous utilisons la commande :
```bash
kubectl exec -it $POD_NAME bash
```

Une fois à l'intérieur de l'application, nous pouvons vérifier l'état de l'application en cours d'exécution avec la commande :
```bash
curl localhost:8080
```

Pour sortir du pod de l'application, nous utilisons la commande :
```bash
exit
```

Autres commandes Kubernetes utiles :

```bash
kubectl get nodes

kubectl create -f hello-app.yaml

kubectl proxy --address='0.0.0.0' --accept-hosts='^*$'
```

Pour vérifier la version de Kubernetes :
```bash
kubeadm version
```

Version du client : version.Info{Major:"1", Minor:"19", GitVersion:"v1.19.3", GitCommit:"1e11e4a2108024935ecfcb2912226cedeafd99df", GitTreeState:"clean", BuildDate:"2020-10-14T12:50:19Z", GoVersion:"go1.15.2", Compiler:"gc", Platform:"linux/amd64"}

Version du serveur : version.Info{Major:"1", Minor:"19", GitVersion:"v1.19.3", GitCommit:"1e11e4a2108024935ecfcb2912226cedeafd99df", GitTreeState:"clean", BuildDate:"2020-10-14T12:41:49Z", GoVersion:"go1.15.2", Compiler:"gc", Platform:"linux/amd64"}

```bash
[root@master1181 ~]# kubectl version
Client Version: version.Info{Major:"1", Minor:"19", GitVersion:"v1.19.3", GitCommit:"1e11e4a2108024935ecfcb2912226cedeafd99df", GitTreeState:"clean", BuildDate:"2020-10-14T12:50:19Z", GoVersion:"go1.15.2", Compiler:"gc", Platform:"linux/amd64"}

Server Version: version.Info{Major:"1", Minor:"19", GitVersion:"v1.19.3", GitCommit:"1e11e4a2108024935ecfcb2912226cedeafd99df", GitTreeState:"clean", BuildDate:"2020-10-14T12:41:49Z", GoVersion:"go1.15.2", Compiler:"gc", Platform:"linux/amd64"}

[root@master1181 ~]# docker --version
Docker version 19.03.13, build 4484c46d9d

[root@master1181 ~]# curl http://localhost:8001/version -k
{
  "major": "1",
  "minor": "19",
  "gitVersion": "v1.19.3",
  "gitCommit": "1e11e4a2108024935ecfcb2912226cedeafd99df",
  "gitTreeState": "clean",
  "buildDate": "2020-10-14T12:41:49Z",
  "goVersion": "go1.15.2",
  "compiler": "gc",
  "platform": "linux/amd64"
}[root@master1181 ~]# 
```

