# Manipulation des Deployments Kubernetes (K8s)

## Création d'un nouveau Deployment

```bash
kubectl create deployments nginx --image=nginx
```

Résultat :
```bash
[root@master1181 ~]# kubectl create deployments nginx --image=nginx 
deployment.apps/nginx créé

[root@master1181 ~]# kubectl get pods 
NOM                      PRÊT   STATUT               REDEMARRES   ÂGE
nginx-6799fc88d8-7rvzp   0/1     En création de conteneur  0          6s

[root@master1181 ~]# kubectl get pods 
NOM                      PRÊT   STATUT    REDEMARRES   ÂGE
nginx-6799fc88d8-7rvzp   1/1     En cours d'exécution  0          108s
```

## Obtenir la liste des Deployments

```bash
kubectl get deployments
```

Résultat :
```bash
[root@master1181 ~]# kubectl get deployments
NOM    PRÊT   À JOUR   DISPONIBLE   ÂGE
nginx   1/1     1            1           3m31s
```

## Afficher la description du Deployment

```bash
kubectl describe deployments nginx
```

Résultat :
```bash
[root@master1181 ~]# kubectl describe deployments nginx
Nom:                   nginx
Espace de noms:        par défaut
Date de création:      Ven, 30 Oct 2020 16:44:32 +0700
Étiquettes:            app=nginx
Annotations:           deployment.kubernetes.io/revision: 1
Sélecteur:             app=nginx
Réplicas:              1 souhaité | 1 mis à jour | 1 total | 1 disponible | 0 indisponible
Type de Stratégie:     Mise à jour progressive
Secondes Min. Prêtes:  0
Stratégie de Mise à Jour Progressive:  25% max indisponible, 25% max en plus
Modèle de Pod:
  Étiquettes:  app=nginx
  Conteneurs:
   nginx:
    Image:        nginx
    Port:         <aucun>
    Port hôte:    <aucun>
    Environnement:  <aucun>
    Montages:       <aucun>
  Volumes:        <aucun>
Conditions:
  Type           Statut  Raison
  ----           ------  ------
  Disponible      Vrai    Nombre minimum de réplicas disponibles
  En progression  Vrai    Nouveau ReplicaSet disponible
Anciens ReplicaSets:  <aucun>
Nouveau ReplicaSet:   nginx-6799fc88d8 (1/1 réplicas créés)
Événements:
  Type    Raison             Âge    De                   Message
  ----    ------             ----   ----                 -------
  Normal  ScalingReplicaSet  4m28s  deployment-controller  Replica set nginx-6799fc88d8 monté à 1
```

## Suppression du Deployment

```bash
kubectl delete deployments nginx
```

Résultat :
```bash
[root@master1181 ~]# kubectl delete deployments nginx
deployment.apps "nginx" supprimé
```

# Gestion de Minikube et Kubernetes

Après avoir démarré Minikube avec succès, nous utilisons la commande suivante pour obtenir des informations générales sur le cluster Kubernetes :
```bash
kubectl cluster-info
```

Pour voir les informations des machines Master et Node dans le cluster Kubernetes, nous utilisons la commande :
```bash
kubectl get nodes
```

Pour vérifier si l'application a été déployée avec succès, nous utilisons la commande :
```bash
kubectl get deployments
```

Pour vérifier si l'application fonctionne, nous ouvrons un autre terminal et saisissons la commande :
```bash
kubectl proxy
```

Pour lister les pods, nous utilisons la commande :
```bash
kubectl get pods
```

Pour voir plus de détails sur les pods en cours d'exécution, tels que l'image du pod, les variables d'environnement, les ressources déclarées, etc., nous utilisons la commande :
```bash
kubectl describe pods
```

Pour voir les logs de l'application en cours d'exécution, nous utilisons la commande :
```bash
kubectl logs $POD_NAME
```

Pour aller plus loin dans l'application et exécuter une commande sur l'application, nous utilisons :
```bash
kubectl exec $POD_NAME env
```

Pour initialiser un shell bash du pod de l'application, nous utilisons :
```bash
kubectl exec -it $POD_NAME bash
```

À ce stade, nous sommes à l'intérieur de l'application et nous pouvons vérifier son fonctionnement avec la commande :
```bash
curl localhost:8080
```

Pour sortir du pod de l'application, nous utilisons la commande :
```bash
exit
```

## Création de Service

Vérification de l'état de l'application :
```bash
kubectl get pods
```

Vérification des Services créés :
```bash
kubectl get services
```

Exposition de l'application Pod via un Service :
```bash
kubectl expose deployment/kubernetes-bootcamp --type="NodePort" --port 8080
```

Vérification du Service récemment créé :
```bash
kubectl get services
```

Détails supplémentaires sur le Service :
```bash
kubectl describe services/kubernetes-bootcamp
```

Visualisation du port publié sur Minikube :
```bash
export NODE_PORT=$(kubectl get services/kubernetes-bootcamp -o go-template='{{(index .spec.ports 0).nodePort}}')
```

Utilisation de curl pour vérifier le succès de l'utilisation du Service :
```bash
curl $(minikube ip):$NODE_PORT
```

## Utilisation des Labels
Affichage des Labels de l'application :
```bash
kubectl describe deployment
```

Vérification des Labels sur le Pod :
```bash
kubectl get pods -l run=kubernetes-bootcamp
```

Vérification des Labels sur le Service :
```bash
kubectl get services -l run=kubernetes-bootcamp
```

Récupération du nom du Pod en cours d'exécution :
```bash
export POD_NAME=$(kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
```

Attribution de Labels au Pod :
```bash
kubectl label pod $POD_NAME app=v1
```

Vérification des Labels attribués :
```bash
kubectl describe pods $POD_NAME
```

ou par la commande :
```bash
kubectl get pods -l app=v1
```

## Suppression du Service
Parfois, nous devons également supprimer un Service pour des erreurs de création ou parce qu'il n'est plus nécessaire. Pour cela, nous utilisons la commande :
```bash
kubectl delete service -l run=kubernetes-bootcamp
```

Vérification que le Service a été supprimé :
```bash
kubectl get services
```

## Extension de l'application

Vérification de l'application en cours d'exécution :
```bash
kubectl get deployments
```

L'application s'exécute actuellement avec une seule réplique, nous allons maintenant l'étendre à 4 répliques :
```bash
kubectl scale deployments/kubernetes-bootcamp --replicas=4
```



