# Guide d'installation de MetalLB sur K8s

## Préparation

Déployer un cluster de 3 nœuds selon le document :
- [Guide d'installation de Kubernetes (utilisant Kubeadmin) sur CentOS 7](/docs/setup/install-k8s-centos7-kubeadm.md)

Déployer NFS comme stockage pour le cluster K8s selon le document :
- [Guide d'installation de NFS comme stockage pour K8s](/docs/setup/install-nfs-storage-k8s.md)

Pour installer MetalLB, il faut respecter certaines conditions :
- Version du cluster Kubernetes à partir de 1.13.0, le cluster ne supporte pas encore le load-balancing réseau
- Des adresses IPv4 libres pour que MetalLB puisse les attribuer

Pour en savoir plus, consultez :
- https://metallb.universe.tf/#requirements
- https://metallb.universe.tf/installation/network-addons/


## Partie 1. Installation de MetalLB

### Étape 1 : Télécharger le fichier manifest

```bash
mkdir -p /root/metalb
cd /root/metalb
curl -fsSL -o namespace.yaml https://raw.githubusercontent.com/metallb/metallb/v0.9.4/manifests/namespace.yaml
curl -fsSL -o metallb.yaml https://raw.githubusercontent.com/metallb/metallb/v0.9.4/manifests/metallb.yaml
```

Résultat :
```bash
[root@master1181 metalb]# ll
total 16
-rw-r--r-- 1 root root 7527 09:50  3 Nov metallb.yaml
-rw-r--r-- 1 root root   91 09:50  3 Nov namespace.yaml
```

### Étape 2 : Installer MetalLB
```bash
kubectl apply -f namespace.yaml
kubectl apply -f metallb.yaml
```

### Étape 3 : Créer un secret pour MetalLB
```bash
kubectl create secret generic -n metallb-system memberlist --from-literal=secretkey="$(openssl rand -base64 128)"
```

### Étape 4 : Créer le fichier de configuration pour MetalLB

Notez que :
- Le cluster K8s a les adresses IP des nœuds dans la plage 10.10.11.x
- Nous allons configurer MetalLB pour attribuer des IP dans la même plage que les nœuds du cluster K8s
- Les adresses IP configurées dans la section `addresses` sont les IP EXTERNES attribuées aux services

Pour en savoir plus, consultez : https://metallb.universe.tf/configuration/

Créer un nouveau fichier `metallb-config.yaml` avec le contenu suivant :
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  namespace: metallb-system
  name: config
data:
  config: |
    address-pools:
    - name: default
      protocol: layer2
      addresses:
      - 10.10.11.94-10.10.11.98
```

Résultat :
```bash
[root@master1181 metalb]# cat metallb-config.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  namespace: metallb-system
  name: config
data:
  config: |
    address-pools:
    - name: default
      protocol: layer2
      addresses:
      - 10.10.11.94-10.10.11.98
```

Appliquer la configuration :
```bash
kubectl apply -f metallb-config.yaml
```

Résultat :
```bash
[root@master1181 metalb]# kubectl apply -f metallb-config.yaml
configmap/config created
```

À ce stade, MetalLB est installé et configuré avec succès.

## Partie 2. Vérification

### Étape 1 : Créer un nouveau déploiement et services

Créer un nouveau fichier `kube-demo-lb.yaml` avec le contenu suivant :
```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: kube-demo-lb
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: kube-demo-lb
  namespace: kube-demo-lb
  labels:
    app: kube-demo-lb
spec:
  replicas: 1
  selector:
    matchLabels:
      app: kube-demo-lb
  template:
    metadata:
      labels:
        app: kube-demo-lb
    spec:
      containers:
      - name: nginx
        image: nginx:1.14.2
        ports:
        - containerPort: 80
---
apiVersion: v1
kind: Service
metadata:
  name: kube-demo-lb
  namespace: kube-demo-lb
spec:
  selector:
    app: kube-demo-lb
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
  type: LoadBalancer
```

Appliquer le fichier manifest :
```bash
kubectl apply -f kube-demo-lb.yaml
```

### Étape 2 : Vérifier

Vérifier les déploiements et les pods :
```bash
kubectl get deployments -n kube-demo-lb
kubectl get pods -n kube-demo-lb
```

Résultats :
```bash
[root@master1181 metalb]# kubectl get deployments -n kube-demo-lb
NAME           READY   UP-TO-DATE   AVAILABLE   AGE
kube-demo-lb   1/1     1            1           89s

[root@master1181 metalb]# kubectl get pods -n kube-demo-lb
NAME                            READY   STATUS    RESTARTS   AGE
kube-demo-lb-6fc6776fcc-t2jcp   1/1     Running   0          74s
```

Vérifier les services :
```bash
kubectl get services -n kube-demo-lb
kubectl describe services kube-demo-lb -n kube-demo-lb
```

Résultats :
```bash
[root@master1181 metalb]# kubectl get services -n kube-demo-lb
NAME           TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)        AGE
kube-demo-lb   LoadBalancer   10.111.135.182   10.10.11.94   80:31399/TCP   47s

[root@master1181 metalb]# kubectl describe services kube-demo-lb -n kube-demo-lb
Name:                     kube-demo-lb
Namespace:                kube-demo-lb
Labels:                   <none>
Annotations:              <none>
Selector:                 app=kube-demo-lb
Type:                     LoadBalancer
IP:                       10.111.135.182
LoadBalancer Ingress:     10.10.11.94
Port:                     <unset>  80/TCP
TargetPort:               80/TCP
NodePort:                 <unset>  31399/TCP
Endpoints:                10.244.1.2:80
Session Affinity:         None
External Traffic Policy:  Cluster
Events:
  Type    Reason        Age    From                Message
  ----    ------        ----   ----                -------
  Normal  IPAllocated   3m7s   metallb-controller  Assigned IP "10.10.11.94"
  Normal  nodeAssigned  2m52s  metallb-speaker     announcing from node "worker1182"
```

À ce stade, nous pouvons voir que le service `kube-demo-lb` a une IP EXTERNE de 10.10.11.94.

Accédez au Web à l'adresse http://10.10.11.94/

![](/images/setup/install-metallb/pic1.png)

À ce stade, le cluster K8s prend en charge LB (MetalLB) et peut attribuer des IP EXTERNES aux services.

# Références

https://metallb.universe.tf/configuration/

https://opensource.com/article/20/7/homelab-metallb

