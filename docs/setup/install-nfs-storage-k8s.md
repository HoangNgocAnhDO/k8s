# Guide d'installation de NFS comme stockage pour K8s

## Préparation

Déployer un cluster K8s de 3 nœuds selon le document :
- [Guide d'installation de Kubernetes (utilisant Kubeadmin) sur CentOS 7](/docs/setup/install-k8s-centos7-kubeadm.md)

Préparer un nœud de stockage offrant le service NFS avec les exigences suivantes :
- Fonctionne sous le système d'exploitation CentOS 7
- Adresse : 10.10.11.99
- Configuration : 2 CPU, 2 GB RAM
- Disque : 100 GB

Note :
- Assurez-vous que le nœud NFS permet à tous les nœuds du cluster K8s de se connecter

## Installation de NFS sur le nœud

### Étape 1 : Configuration de NTP
```
timedatectl set-timezone Asia/Ho_Chi_Minh

yum -y install chrony
systemctl enable chronyd.service
systemctl restart chronyd.service
chronyc sources
```

### Étape 2 : Installation de NFS

```
yum install nfs-utils rsync -y
systemctl enable rpcbind
systemctl enable nfs-server
systemctl enable nfs-lock
systemctl enable nfs-idmap
systemctl start rpcbind
systemctl start nfs-server
systemctl start nfs-lock
systemctl start nfs-idmap
```

### Étape 3 : Création d'un nouveau dossier de partage

```
mkdir /storagek8s
echo '/storagek8s  10.10.11.81(rw,sync,no_root_squash,no_all_squash) 10.10.11.82(rw,no_root_squash) 10.10.11.83(rw,no_root_squash)' > /etc/exports
```

### Étape 4 : Exporter le dossier `/storagek8s` via NFS

```
exportfs -ra
```

Ou vous pouvez redémarrer le service NFS

```
systemctl restart nfs-server
```

### Étape 5 : Vérification

```
[root@nfs1199 ~]# exportfs -v
/storagek8s   	10.10.11.81(sync,wdelay,hide,no_subtree_check,sec=sys,rw,secure,no_root_squash,no_all_squash)
/storagek8s   	10.10.11.82(sync,wdelay,hide,no_subtree_check,sec=sys,rw,secure,no_root_squash,no_all_squash)
/storagek8s   	10.10.11.83(sync,wdelay,hide,no_subtree_check,sec=sys,rw,secure,no_root_squash,no_all_squash)
```

À ce stade, le nœud NFS peut être utilisé comme stockage pour le cluster K8s.

## Partie 2 : Installation du client NFS

> À réaliser sur tous les nœuds du cluster K8s

### Étape 1 : Installation du paquet client NFS
```
yum install nfs-utils -y
```

### Étape 2 : Vérification du chemin de montage

```
showmount -e 10.10.11.99
```

Résultat

```
[root@master1181 ~]# showmount -e 10.10.11.99
Liste des exports pour 10.10.11.99 :
/storagek8s 10.10.11.83,10.10.11.82,10.10.11.81
```

À partir de maintenant, NFS peut être utilisé comme stockage pour K8s.

## Sources

https://news.cloud365.vn/nfs-network-file-system/

