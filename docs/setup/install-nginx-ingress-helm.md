# Guide d'installation et d'utilisation du Nginx Ingress Controller

## Préparation

Avoir lu l'aperçu de Ingress 
- [Concept de Ingress](/docs/2.9-ingress-k8s.md)

Déployer un cluster de 3 nœuds selon le document :
- [Guide d'installation de Kubernetes (utilisant Kubeadmin) sur CentOS 7](/docs/setup/install-k8s-centos7-kubeadm.md)

Déployer NFS comme stockage pour le cluster K8s selon le document :
- [Guide d'installation de NFS comme stockage pour K8s](/docs/setup/install-nfs-storage-k8s.md)

Installer Helm sur le cluster K8s
- [Guide d'installation et d'utilisation de Helm](/docs/setup/install-wp-helm.md)

Avoir installé MetalLB selon le document
- [Guide de déploiement de MetalLB pour le cluster K8s](/docs/setup/install-metallb.md)

## Qu'est-ce que le Ingress Controller ?

Le Ingress Controller est une application qui s'exécute dans un cluster et utilise la configuration du LoadBalancer HTTP selon la ressource Ingress. Ce LoadBalancer peut être un logiciel exécuté dans le cluster (MetalLB), un LoadBalancer matériel, ou un LoadBalancer de service cloud externe. Chaque type de LoadBalancer nécessite une mise en œuvre différente du Ingress Controller.

Dans ce cas, le Ingress Controller est déployé sous forme de logiciel.

![](/images/setup/install-nginx-ingress-helm/pic1.png)

## Partie 1 : Installation de Nginx Ingress

### Étape 1 : Créer un nouveau dossier pour le fichier manifest

```
mkdir -p /root/helm-nginx-ingress
cd /root/helm-nginx-ingress
```

### Étape 2 : Ajouter le Repo `nginx-stable`

Remarque :
- Si le repo `nginx-stable` existe déjà, vous pouvez ignorer cette étape

Exécuter
```
helm repo add nginx-stable https://helm.nginx.com/stable
```

Résultat
```
[root@master1181 helm-nginx-ingress]# helm repo add nginx-stable https://helm.nginx.com/stable
"nginx-stable" a été ajouté à vos dépôts
```

### Étape 3 : Installer le Nginx Ingress Controller

Exécuter
```
helm install k8s-ingress nginx-stable/nginx-ingress
```

Résultat
```
[root@master1181 helm-nginx-ingress]# helm install k8s-ingress nginx-stable/nginx-ingress
W1104 11:09:58.929751    5386 warnings.go:67] apiextensions.k8s.io/v1beta1 CustomResourceDefinition is déprécié dans v1.16+, indisponible dans v1.22+; utiliser apiextensions.k8s.io/v1 CustomResourceDefinition
...
NAME: k8s-ingress
LAST DEPLOYED: Wed Nov  4 11:10:02 2020
NAMESPACE: default
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
Le NGINX Ingress Controller a été installé.
```

### Étape 4 : Vérifier le service Nginx Ingress

Exécuter =
```
kubectl get deployments
kubectl get pods
kubectl get services
```

Résultat
```
[root@master1181 helm-nginx-ingress]# kubectl get deployments
NAME                        READY   UP-TO-DATE   AVAILABLE   AGE
k8s-ingress-nginx-ingress   1/1     1            1           83s

[root@master1181 helm-nginx-ingress]# kubectl get pods
NAME                                         READY   STATUS    RESTARTS   AGE
k8s-ingress-nginx-ingress-68467f5768-2zcqx   1/1     Running   0          95s

[root@master1181 helm-nginx-ingress]# kubectl get services
NAME                        TYPE           CLUSTER-IP      EXTERNAL-IP   PORT(S)                      AGE
k8s-ingress-nginx-ingress   LoadBalancer   10.106.37.130   10.10.11.94   80:30307/TCP,443:31958/TCP   69s
kubernetes                  ClusterIP      10.96.0.1       <none>        443/TCP                      2d2h
```
Selon le résultat, les services `k8s-ingress-nginx-ingress` ont été exposés via l'adresse `EXTERNAL-IP` qui est `10.10.11.94`

Accédez via le navigateur et vérifiez

![](/images/setup/install-nginx-ingress-helm/pic2.png)

À ce stade, l'installation de Nginx Ingress a été un succès.

## Partie 2: Exposer les services via Nginx Ingress

### Étape 1: Créer des Deployments et Services

#### Créer un nouveau fichier contenant les Deployments et services `hello-app-1` nommé `hello-app-1.yml`

Contenu du fichier `hello-app-1.yml`
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: hello-app-1
spec:
  replicas: 1
  selector:
    matchLabels:
      app: hello-app-1
  template:
    metadata:
      labels:
        app: hello-app-1
    spec:
      containers:
      - image: gcr.io/google-samples/hello-app:1.0
        imagePullPolicy: Always
        name: hello-app-1
        ports:
        - containerPort: 8080
---
apiVersion: v1
kind: Service
metadata:
  name: hello-app-1-services
spec:
  type: ClusterIP
  ports:
    - port: 8080
      targetPort: 8080
      protocol: TCP
  selector:
    app: hello-app-1
```

Résultat
```
[root@master1181 helm-nginx-ingress]# cat hello-app-1.yml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: hello-app-1
spec:
  replicas: 1
  selector:
    matchLabels:
      app: hello-app-1
  template:
    metadata:
      labels:
        app: hello-app-1
    spec:
      containers:
      - image: gcr.io/google-samples/hello-app:1.0
        imagePullPolicy: Always
        name: hello-app-1
        ports:
        - containerPort: 8080
---
apiVersion: v1
kind: Service
metadata:
  name: hello-app-1-services
spec:
  type: ClusterIP
  ports:
    - port: 8080
      targetPort: 8080
      protocol: TCP
  selector:
    app: hello-app-1
```

Créer une nouvelle ressource
```
kubectl apply -f hello-app-1.yml
```

Résultat
```
[root@master1181 helm-nginx-ingress]# kubectl apply -f hello-app-1.yml 
deployment.apps/hello-app-1 created
service/hello-app-1-services created
```

#### Créer un nouveau fichier contenant les Deployments et services `hello-app-2` nommé `hello-app-2.yml`

Contenu du fichier `hello-app-2.yml`
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: hello-app-2
spec:
  replicas: 1
  selector:
    matchLabels:
      app: hello-app-2
  template:
    metadata:
      labels:
        app: hello-app-2
    spec:
      containers:
      - image: gcr.io/google-samples/hello-app:2.0
        imagePullPolicy: Always
        name: hello-app-2
        ports:
        - containerPort: 8080
---
apiVersion: v1
kind: Service
metadata:
  name: hello-app-2-services
spec:
  type: ClusterIP
  ports:
    - port: 8080
      targetPort: 8080
      protocol: TCP
  selector:
    app: hello-app-2
```

Résultat
```
[root@master1181 helm-nginx-ingress]# cat hello-app-2.yml 
apiVersion: apps/v1
kind: Deployment
metadata:
  name: hello-app-2
spec:
  replicas: 1
  selector:
    matchLabels:
      app: hello-app-2
  template:
    metadata:
      labels:
        app: hello-app-2
    spec:
      containers:
      - image: gcr.io/google-samples/hello-app:2.0
        imagePullPolicy: Always
        name: hello-app-2
        ports:
        - containerPort: 8080
---
apiVersion: v1
kind: Service
metadata:
  name: hello-app-2-services
spec:
  type: ClusterIP
  ports:
    - port: 8080
      targetPort: 8080
      protocol: TCP
  selector:
    app: hello-app-2
```

Créer une nouvelle ressource
```
kubectl apply -f hello-app-2.yml
```

Résultat
```
[root@master1181 helm-nginx-ingress]# kubectl apply -f hello-app-2.yml
deployment.apps/hello-app-2 created
service/hello-app-2-services created
```

### Étape 2: Vérifier les Deployments et Services créés

Exécuter
```
kubectl get pods
kubectl get deployments
kubectl get services
```

Résultats

```
[root@master1181 helm-nginx-ingress]# kubectl get pods 
NAME                                         READY   STATUS    RESTARTS   AGE
hello-app-1-675b9f655c-7pwvs                 1/1     Running   0          3m41s
hello-app-2-787d8656bb-kbrps                 1/1     Running   0          90s

[root@master1181 helm-nginx-ingress]# kubectl get deployments
NAME                        READY   UP-TO-DATE   AVAILABLE   AGE
hello-app-1                 1/1     1            1           3m53s
hello-app-2                 1/1     1            1           102s
k8s-ingress-nginx-ingress   1/1     1            1           16m

[root@master1181 helm-nginx-ingress]# kubectl get services
NAME                        TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)                      AGE
hello-app-1-services        ClusterIP      10.96.20.183     <none>        8080/TCP                     4m2s
hello-app-2-services        ClusterIP      10.111.235.190   <none>        8080/TCP                     111s
k8s-ingress-nginx-ingress   LoadBalancer   10.106.37.130    10.10.11.94   80:30307/TCP,443:31958/TCP   16m
kubernetes                  ClusterIP      10.96.0.1        <none>        443/TCP                      2d2h
```

Selon les résultats, les services `hello-app-1-services` et `hello-app-2-services` sont de type `ClusterIP` et exposent le port `8080`

Vérifier
```
curl 10.96.20.183:8080
curl 10.111.235.190:8080
```

Résultats
```
[root@master1181 helm-nginx-ingress]# curl 10.96.20.183:8080
Hello, world!
Version: 1.0.0
Hostname: hello-app-1-675b9f655c-7pwvs
[root@master1181 helm-nginx-ingress]# curl 10.111.235.190:8080
Hello, world!
Version: 2.0.0
Hostname: hello-app-2-787d8656bb-kbrps
```

### Étape 3: Exposer les services via Nginx Ingress

Créer un nouveau fichier `hello-app-ingress.yaml`

Contenu
```
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: nginx
  name: hello-app-ingress
spec:
  rules:
    - host: hello-app-1.local
      http:
        paths:
          - backend:
              serviceName: hello-app-1-services
              servicePort: 8080
            path: /
    - host: hello-app-2.local
      http:
        paths:
          - backend:
              serviceName: hello-app-2-services
              servicePort: 8080
            path: /
```

Résultat
```
[root@master1181 helm-nginx-ingress]# cat hello-app-ingress.yaml 
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: nginx
  name: hello-app-ingress
spec:
  rules:
    - host: hello-app-1.local
      http:
        paths:
          - backend:
              serviceName: hello-app-1-services
              servicePort: 8080
            path: /
    - host: hello-app-2.local
      http:
        paths:
          - backend:
              serviceName: hello-app-2-services
              servicePort: 8080
            path: /
```

Créer à nouveau Ingress
```
kubectl apply -f hello-app-ingress.yaml
```

Résultat
```
[root@master1181 helm-nginx-ingress]# kubectl apply -f hello-app-ingress.yaml
Warning: networking.k8s.io/v1beta1 Ingress is deprecated in v1.19+, unavailable in v1.22+; use networking.k8s.io/v1 Ingress
ingress.networking.k8s.io/example created
```

Vérifier
```
kubectl get ingress
kubectl describe ingress hello-app-ingress
```

Résultat
```
[root@master1181 helm-nginx-ingress]# kubectl get ingress
Warning: extensions/v1beta1 Ingress is deprecated in v1.14+, unavailable in v1.22+; use networking.k8s.io/v1 Ingress
NAME                CLASS    HOSTS                                 ADDRESS       PORTS   AGE
hello-app-ingress   <none>   hello-app-1.local,hello-app-2.local   10.10.11.94   80      8s

[root@master1181 helm-nginx-ingress]# kubectl describe ingress hello-app-ingress
Warning: extensions/v1beta1 Ingress is deprecated in v1.14+, unavailable in v1.22+; use networking.k8s.io/v1 Ingress
Name:             hello-app-ingress
Namespace:        default
Address:          10.10.11.94
Default backend:  default-http-backend:80 (<error: endpoints "default-http-backend" not found>)
Rules:
  Host               Path  Backends
  ----               ----  --------
  hello-app-1.local  
                     /   hello-app-1-services:8080   10.244.1.4:8080)
  hello-app-2.local  
                     /   hello-app-2-services:8080   10.244.2.5:8080)
Annotations:         kubernetes.io/ingress.class: nginx
Events:
  Type    Reason          Age   From                      Message
  ----    ------          ----  ----                      -------
  Normal  AddedOrUpdated  24s   nginx-ingress-controller  Configuration for default/hello-app-ingress was added or updated
```

### Etape 4: Vérification

Dans la machine, ajoutez ces 2 lignes dans `/etc/hosts`
```
thanhnb@thanhnb:~$ cat /etc/hosts
10.10.11.94 hello-app-1.local
10.10.11.94 hello-app-2.local
```

Accédez http://hello-app-1.local và http://hello-app-2.local

Résultat

![](/images/setup/install-nginx-ingress-helm/pic3.png)

![](/images/setup/install-nginx-ingress-helm/pic4.png)


## Sources

https://kubernetes.github.io/ingress-nginx/deploy/#using-helm

https://viblo.asia/p/su-dung-kubernetes-ingress-nginx-ingress-controller-de-dinh-tuyen-nhieu-service-khac-nhau-L4x5x8Gg5BM
