# Guide de déploiement de Helm sur K8s

## Préparation

Avoir lu un aperçu sur les Helm Charts
- [Concepts de Helm dans Kubernetes](/docs/setup/install-helm-k8s.md)

Déployer un cluster de 3 nœuds selon le document :
- [Guide d'installation de Kubernetes (utilisation de Kubeadmin) sur CentOS 7](/docs/setup/install-k8s-centos7-kubeadm.md)

Déployer NFS comme stockage pour le cluster K8s selon le document :
- [Guide d'installation de NFS comme stockage pour K8s](/docs/setup/install-nfs-storage-k8s.md)

Installer Helm sur le cluster K8s
- [Guide d'installation et d'utilisation de Helm](/docs/setup/install-wp-helm.md)

## Partie 1 : Création d'une partition de stockage de données pour Wordpress

> À réaliser sur le serveur NFS

Note :
- Lors de l'utilisation de NFS comme stockage partagé pour K8s, les données des Pods seront montées sur la partition NFS en tant que Volume Persistant.
- Lors du déploiement de Wordpress sur K8s, nous devrons créer une nouvelle partition pour les données de Wordpress sur le serveur NFS.

### Étape 1 : Créer un nouveau chemin de montage NFS

```
cd /storagek8s
mkdir helm_wpdata1
mkdir helm_wpdata2
```

### Étape 2 : Attribuer les droits aux dossiers

```
chown -R nfsnobody:nfsnobody /storagek8s/helm_wpdata1
chown -R nfsnobody:nfsnobody /storagek8s/helm_wpdata2
chmod -R 777 /storagek8s/helm_wpdata1
chmod -R 777 /storagek8s/helm_wpdata2
```

Résultat
```
[root@nfs1199 storagek8s]# chown -R nfsnobody:nfsnobody /storagek8s/helm_wpdata1
[root@nfs1199 storagek8s]# chown -R nfsnobody:nfsnobody /storagek8s/helm_wpdata2
[root@nfs1199 storagek8s]# chmod -R 777 /storagek8s/helm_wpdata1
[root@nfs1199 storagek8s]# chmod -R 777 /storagek8s/helm_wpdata2
[root@nfs1199 storagek8s]# ll
total 0
drwxrwxrwx 2 nfsnobody nfsnobody 6 13:59  2 Nov helm_wpdata1
drwxrwxrwx 2 nfsnobody nfsnobody 6 13:40  2 Nov helm_wpdata2
```

## Partie 2 : Déploiement de Wordpress avec Helm

> À réaliser sur le nœud Master du cluster k8s, en tant que `root`

### Étape 1 : Créer un nouveau dossier pour les fichiers manifest

```
mkdir -p /root/helm-wp-k8s
cd /root/helm-wp-k8s
```

### Étape 2 : Créer un nouveau Volume Persistant

Créer le fichier PV `helm-wordpress-pv.yml`
```
[root@master1181 helm-wp-k8s]# cat helm-wordpress-pv.yml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: wordpress-helm-data-1
spec:
  capacity:
    storage: 20Gi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteOnce
  nfs:
    path: /storagek8s/helm_wpdata1
    server: 10.10.11.99
    readOnly: false
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: wordpress-helm-data-2
spec:
  capacity:
    storage: 20Gi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteOnce
  nfs:
    path: /storagek8s/helm_wpdata2
    server: 10.10.11.99
    readOnly: false
```

Créer le PV
```
kubectl apply -f helm-wordpress-pv.yml
```

Résultats
```
[root@master1181 helm-wp-k8s]# kubectl apply -f helm-wordpress-pv.yml
persistentvolume/wordpress-helm-data-1 created
persistentvolume/wordpress-helm-data-2 created
```

Vérification
```
[root@master1181 helm-wp-k8s]# kubectl get pv
NAME                    CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS      CLAIM   STORAGECLASS   REASON   AGE
wordpress-helm-data-1   20Gi       RWO            Retain           Available                                   5s
wordpress-helm-data-2   20Gi       RWO            Retain           Available                                   5s
```

### Étape 3: Ajouter le dépôt Bitnami

Note:
- Si le dépôt Bitnami existe déjà, vous pouvez ignorer cette étape.

Exécution
```
helm repo add bitnami https://charts.bitnami.com/bitnami
```

Résultat
```
[root@master1181 helm-wp-k8s]# helm repo add bitnami https://charts.bitnami.com/bitnami
"bitnami" has been added to your repositories

[root@master1181 helm-wp-k8s]# helm repo ls
NAME   	URL                               
bitnami	https://charts.bitnami.com/bitnami
```

Recherche du helm chart wordpress
```
helm search repo bitnami
```

Résultat
```
[root@master1181 helm-wp-k8s]# helm search repo bitnami | grep wordpress
bitnami/wordpress               	9.9.1        	5.5.3        	Web publishing platform for building blogs and ...
```

### Étape 4: Installer Wordpress
```
helm install helm-wp bitnami/wordpress
```

Résultat
```
[root@master1181 helm-wp-k8s]# helm install helm-wp bitnami/wordpress
NAME: helm-wp
LAST DEPLOYED: Mon Nov  2 13:55:01 2020
NAMESPACE: default
STATUS: deployed
REVISION: 1
NOTES:
** Please be patient while the chart is being deployed **

Your WordPress site can be accessed through the following DNS name from within your cluster:

    helm-wp-wordpress.default.svc.cluster.local (port 80)

To access your WordPress site from outside the cluster follow the steps below:

1. Get the WordPress URL by running these commands:

  NOTE: It may take a few minutes for the LoadBalancer IP to be available.
        Watch the status with: 'kubectl get svc --namespace default -w helm-wp-wordpress'

   export SERVICE_IP=$(kubectl get svc --namespace default helm-wp-wordpress --template "{{ range (index .status.loadBalancer.ingress 0) }}{{.}}{{ end }}")
   echo "WordPress URL: http://$SERVICE_IP/"
   echo "WordPress Admin URL: http://$SERVICE_IP/admin"

2. Open a browser and access WordPress using the obtained URL.

3. Login with the following credentials below to see your blog:

  echo Username: user
  echo Password: $(kubectl get secret --namespace default helm-wp-wordpress -o jsonpath="{.data.wordpress-password}" | base64 --decode)
```

Obtenir le mot de passe du compte administrateur Wordpress
```
[root@master1181 helm-wp-k8s]# kubectl get secret --namespace default helm-wp-wordpress -o jsonpath="{.data.wordpress-password}" | base64 --decode
tbfl38Dfku
```

Le compte d'administration Wordpress est `user / tbfl38Dfku`

### Étape 5: Vérifier le service Wordpress

Vérification
```
[root@master1181 helm-wp-k8s]# kubectl get pvc
NAME                     STATUS   VOLUME                  CAPACITY   ACCESS MODES   STORAGECLASS   AGE
data-helm-wp-mariadb-0   Bound    wordpress-helm-data-2   20Gi       RWO                           20s
helm-wp-wordpress        Bound    wordpress-helm-data-1   20Gi       RWO                           20s

[root@master1181 helm-wp-k8s]# kubectl get pods
NAME                                 READY   STATUS    RESTARTS   AGE
helm-wp-mariadb-0                    1/1     Running   0          2m21s
helm-wp-wordpress-7475978ddf-52bm2   1/1     Running   0          2m21s

[root@master1181 helm-wp-k8s]# kubectl get deployments
NAME                READY   UP-TO-DATE   AVAILABLE   AGE
helm-wp-wordpress   1/1     1            1           2m28s

[root@master1181 helm-wp-k8s]# kubectl get services
NAME                TYPE           CLUSTER-IP      EXTERNAL-IP   PORT(S)                      AGE
helm-wp-mariadb     ClusterIP      10.100.86.53    <none>        3306/TCP                     2m42s
helm-wp-wordpress   LoadBalancer   10.106.193.15   <pending>     80:30758/TCP,443:30552/TCP   2m42s
kubernetes          ClusterIP      10.96.0.1       <none>        443/TCP                      5h14m
```
### Étape 6 : Modifier l'exposition du service `helm-wp-wordpress` 

Notez que :
- Modifier le type d'exposition de Wordpress de `LoadBalancer` à `NodePort`

Modifier le type de service pour l'exposition de Wordpress
```bash
kubectl edit services helm-wp-wordpress
```

Modification
```yaml
apiVersion: v1
kind: Service
metadata:
  annotations:
    meta.helm.sh/release-name: helm-wp
    meta.helm.sh/release-namespace: default
  creationTimestamp: "2020-11-02T07:02:04Z"
  labels:
    app.kubernetes.io/instance: helm-wp
    app.kubernetes.io/managed-by: Helm
    app.kubernetes.io/name: wordpress
    helm.sh/chart: wordpress-9.9.1
  name: helm-wp-wordpress
  namespace: default
  resourceVersion: "45848"
  selfLink: /api/v1/namespaces/default/services/helm-wp-wordpress
  uid: ad177c5c-2c71-4c81-8daf-343c719bb089
spec:
  clusterIP: 10.106.193.15
  externalTrafficPolicy: Cluster
  ports:
  - name: http
    nodePort: 30758
    port: 80
    protocol: TCP
    targetPort: http
  - name: https
    nodePort: 30552
    port: 443
    protocol: TCP
    targetPort: https
  selector:
    app.kubernetes.io/instance: helm-wp
    app.kubernetes.io/name: wordpress
  sessionAffinity: None
  type: NodePort # MODIFIER CETTE VALEUR
status:
  loadBalancer: {}
```

Résultat
```bash
[root@master1181 helm-wp-k8s]# kubectl edit services helm-wp-wordpress
service/helm-wp-wordpress edited
```

Vérifier les services
```bash
[root@master1181 helm-wp-k8s]# kubectl get services
NAME                TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)                      AGE
helm-wp-mariadb     ClusterIP   10.100.86.53    <none>        3306/TCP                     17m
helm-wp-wordpress   NodePort    10.106.193.15   <none>        80:30758/TCP,443:30552/TCP   17m
kubernetes          ClusterIP   10.96.0.1       <none>        443/TCP                      5h29m
```

Le service Wordpress est désormais exposé via 2 ports :
- HTTP : 30758
- HTTPS : 30552

### Étape 7 : Accéder à l'interface Wordpress

Accédez à l'URL : http://<IP_K8S_CLUSTER>:30758

Exemple : http://10.10.11.81:30758/

Résultat

![](/images/setup/install-wp-helm/pic1.png)

Connectez-vous au compte administrateur http://10.10.11.81:30758/wp-login.php avec les identifiants `user / tbfl38Dfku`

![](/images/setup/install-wp-helm/pic2.png)

Résultat

![](/images/setup/install-wp-helm/pic3.png)

À ce stade, le déploiement de WordPress avec Helm est terminé.

Pour supprimer WordPress installé via Helm

```bash
[root@master1181 helm-wp-k8s]# helm ls
NAME   	NAMESPACE	REVISION	UPDATED                                	STATUS  	CHART          	APP VERSION
helm-wp	default  	1       	2020-11-02 14:02:04.264079749 +0700 +07	deployed	wordpress-9.9.1	5.5.3      

[root@master1181 helm-wp-k8s]# helm uninstall helm-wp
release "helm-wp" uninstalled

[root@master1181 helm-wp-k8s]# helm ls
NAME	NAMESPACE	REVISION	UPDATED	STATUS	CHART	APP VERSION

[root@master1181 helm-wp-k8s]# kubectl get pods
No resources found in default namespace.

[root@master1181 helm-wp-k8s]# kubectl get deployments
No resources found in default namespace.

[root@master1181 helm-wp-k8s]# kubectl get services
NAME         TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   5h37m
```

À ce stade, nous avons réussi à retirer WordPress du cluster K8s.

