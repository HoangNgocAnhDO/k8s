# Guide d'installation de Traefik sur Ubuntu 20.04

## Vue d'ensemble

Traefik est un reverse-proxy de nouvelle génération, et également un équilibreur de charge pour faciliter le déploiement de systèmes microservices. Il intègre de nombreux composants d'infrastructure tels que Docker, Swarm mode, Kubernetes, Marathon, Consul, Etcd, Rancher, Amazon ECS... Et l'automatisation est l'aspect le plus important dans la configuration avec Traefik.

Pour en savoir plus :
- https://viblo.asia/p/tong-quan-ve-traefik-XL6lAA8Dlek
- https://doc.traefik.io/traefik/

## Préparation

Préparer une VM répondant aux exigences suivantes :
- Utiliser le système d'exploitation Ubuntu 20.04
- Installer les services Docker et Docker Compose
- Disposer d'une IP publique
- Configurer 2 cœurs - 2 Go de RAM - 25 Go de disque
- Nécessaire de pointer 3 domaines vers l'adresse IP publique de la VM avec les préfixes : monitor, wp1, db-admin

La VM a une adresse IP publique : 103.124.94.89 et a déjà pointé `monitor.devopsviet.com, wp1.devopsviet.com, db-admin-wp1.devopsviet.com, wp2.devopsviet.com, db-admin-wp2.devopsviet.com`

## Partie 1 : Préparation des fichiers de configuration de Traefik

### Étape 1 : Créer un répertoire pour les configurations
```
mkdir -p /root/traefik
cd /root/traefik
```

### Étape 2 : Générer le mot de passe de connexion http-auth pour Traefik

```
sudo apt-get -y install apache2-utils
htpasswd -nb admin Cloud365a@123
```

Résultat :
```
root@devtest-traefik:~/traefik# htpasswd -nb admin Cloud365a@123
admin:$apr1$jfEBf99n$/usOROpFWDvM5.U/Gz3oY0
```

Note :
- `admin:$apr1$jfEBf99n$/usOROpFWDvM5.U/Gz3oY0` - Cette valeur sera utilisée dans les étapes suivantes.

### Étape 3 : Créer le fichier de configuration `traefik.toml`

Créer un nouveau fichier `traefik.toml` avec le contenu suivant :
```
# Contenu du fichier traefik.toml à compléter ici
```

```
[entryPoints]
  [entryPoints.web]
    address = ":80"
    [entryPoints.web.http.redirections.entryPoint]
      to = "websecure"
      scheme = "https"

  [entryPoints.websecure]
    address = ":443"
[api]
  dashboard = true
[certificatesResolvers.lets-encrypt.acme]
  email = "thanhnb@nhanhoa.com.vn"
  storage = "acme.json"
  [certificatesResolvers.lets-encrypt.acme.tlsChallenge]
[providers.docker]
  watch = true
  network = "web"
[providers.file]
  filename = "traefik_dynamic.toml"
```

ATTENTION:
-  Section `[certificatesResolvers.lets-encrypt.acme] - email`: Email authentifie let encrypt

Résultat
```
root@devtest-traefik:~/traefix# cat traefik.toml 
[entryPoints]
  [entryPoints.web]
    address = ":80"
    [entryPoints.web.http.redirections.entryPoint]
      to = "websecure"
      scheme = "https"

  [entryPoints.websecure]
    address = ":443"
[api]
  dashboard = true
[certificatesResolvers.lets-encrypt.acme]
  email = "thanhnb@nhanhoa.com.vn"
  storage = "acme.json"
  [certificatesResolvers.lets-encrypt.acme.tlsChallenge]
[providers.docker]
  watch = true
  network = "web"
[providers.file]
  filename = "traefik_dynamic.toml"
```
### Bước 3: Tạo File cấu hình `traefik_dynamic.toml`

Tạo mới file `traefik_dynamic.toml` với với nội dung
```
[http.middlewares.simpleAuth.basicAuth]
  users = [
    "admin:$apr1$jfEBf99n$/usOROpFWDvM5.U/Gz3oY0"
  ]
[http.routers.api]
  rule = "Host(`monitor.devopsviet.com`)"
  entrypoints = ["websecure"]
  middlewares = ["simpleAuth"]
  service = "api@internal"
  [http.routers.api.tls]
    certResolver = "lets-encrypt"
```

Lưu ý:
- `users = [ <User>:<Password> ]`: Khai báo tài khoản Http auth sử dụng login Traefix Dashboard
  - Sử dụng kết quả có được từ `Bước 2` 
- `monitor.devopsviet.com` - Địa chỉ Domain sử dụng để truy cập Traefix Dashboard

Kết quả

```
root@devtest-traefik:~/traefix# cat traefik_dynamic.toml 
[http.middlewares.simpleAuth.basicAuth]
  users = [
    "admin:$apr1$jfEBf99n$/usOROpFWDvM5.U/Gz3oY0"
  ]
[http.routers.api]
  rule = "Host(`monitor.devopsviet.com`)"
  entrypoints = ["websecure"]
  middlewares = ["simpleAuth"]
  service = "api@internal"
  [http.routers.api.tls]
    certResolver = "lets-encrypt"
```

## Partie 2: Créer Traefix Container

### Etape 1: Créer la plage Network nommé `web`
```
docker network create web
```

Résultat
```
root@devtest-traefik:~/traefix# docker network create web
ca17ae38811aab786d9672a0139a268b59656afa8484dcc521cfb9b3bff46e5c
```

### Etape 2: Créer le fichier `acme.json`
```
touch acme.json
chmod 600 acme.json
```

Résultat
```
root@devtest-traefik:~/traefix# ll
total 16
drwxr-xr-x 2 root root 4096 Nov  7 15:13 ./
drwx------ 5 root root 4096 Nov  7 15:11 ../
-rw------- 1 root root    0 Nov  7 15:20 acme.json
-rw-r--r-- 1 root root  487 Nov  7 14:56 traefik.toml
-rw-r--r-- 1 root root  314 Nov  7 15:11 traefik_dynamic.toml
```

### Etape 3: Créer Traefix container
```
docker run -d \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v $PWD/traefik.toml:/traefik.toml \
  -v $PWD/traefik_dynamic.toml:/traefik_dynamic.toml \
  -v $PWD/acme.json:/acme.json \
  -p 80:80 \
  -p 443:443 \
  --network web \
  --name traefik \
  traefik:v2.2
```

Résultat
```
root@devtest-traefik:~/traefix# docker run -d \
>   -v /var/run/docker.sock:/var/run/docker.sock \
>   -v $PWD/traefik.toml:/traefik.toml \
>   -v $PWD/traefik_dynamic.toml:/traefik_dynamic.toml \
>   -v $PWD/acme.json:/acme.json \
>   -p 80:80 \
>   -p 443:443 \
>   --network web \
>   --name traefik \
>   traefik:v2.2
Unable to find image 'traefik:v2.2' locally
v2.2: Pulling from library/traefik
cbdbe7a5bc2a: Pull complete 
f16506d32a25: Pull complete 
605303653d66: Pull complete 
a9005a35b171: Pull complete 
Digest: sha256:ea0aa8832bfd08369166baecd40b35fc58979df8f5dc5182e4e63ee6adbe66db
Status: Downloaded newer image for traefik:v2.2
479ea854f0b004f52a51de19df7db46b8163e67d66e137a7b4d5b1e85807cd0f
```
### Étape 4 : Accéder à l'interface Traefik Dashboard

Accédez à l'URL `https://monitor.your_domain/dashboard/`, dans cet article l'URL utilisée est ``

![](/images/setup/setup-traefik-docker/pic1.png)

Entrez le mot de passe `admin / Cloud365a@123` (voir `Partie 1`) et connectez-vous

Résultat

![](/images/setup/setup-traefik-docker/pic2.png)

## Partie 3 : Utiliser Traefik comme Reverse Proxy pour le service WordPress

### Étape 1 : Initialisation du site WP1

Créez un nouveau dossier pour le site Wordpress 1
```
mkdir -p /root/traefix/wp1
cd /root/traefix/wp1
```

Créez un nouveau fichier docker-compose `docker-compose.yml`


```
version: "3"

networks:
  web:
    external: true
  internal1:
    external: false

services:
  wp1:
    image: wordpress:4.9.8-apache
    environment:
      WORDPRESS_DB_PASSWORD:
    labels:
      - traefik.http.routers.wp1.rule=Host(`wp1.devopsviet.com`)
      - traefik.http.routers.wp1.tls=true
      - traefik.http.routers.wp1.tls.certresolver=lets-encrypt
      - traefik.port=80
    networks:
      - internal1
      - web
    depends_on:
      - mysql
  mysql:
    image: mysql:5.7
    environment:
      MYSQL_ROOT_PASSWORD:
    networks:
      - internal1
    labels:
      - traefik.enable=false

  adminerwp1:
    image: adminer:4.6.3-standalone
    labels:
    labels:
      - traefik.http.routers.adminerwp1.rule=Host(`db-admin-wp1.devopsviet.com`)
      - traefik.http.routers.adminerwp1.tls=true
      - traefik.http.routers.adminerwp1.tls.certresolver=lets-encrypt
      - traefik.port=8080
    networks:
      - internal1
      - web
    depends_on:
      - mysql
```
## Définir le mot de passe de la base de données
```bash
export WORDPRESS_DB_PASSWORD=cloud3652020
export MYSQL_ROOT_PASSWORD=cloud3652020
```

## Démarrer WP 1
```bash
docker-compose up -d
```

Résultat :
```bash
root@devtest-traefik:~/traefix/wp1# docker-compose up -d

Creating network "wp1_internal1" with the default driver
Creating wp1_mysql_1 ... done
Creating wp1_adminerwp1_1 ... done
Creating wp1_wp1_1        ... done
```

Accéder au site wp1 et configurer Wordpress basique. URL : https://wp1.devopsviet.com/

![](/images/setup/setup-traefik-docker/pic3.png)

Résultat

![](/images/setup/setup-traefik-docker/pic4.png)

Accéder à la page d'administration de la base de données : https://db-admin-wp1.devopsviet.com/

Note : 
- Serveur : mysql
- Nom d'utilisateur : root
- Mot de passe : cloud3652020
- Base de données : Laisser vide

![](/images/setup/setup-traefik-docker/pic5.png)

Résultat

![](/images/setup/setup-traefik-docker/pic6.png)

### Étape 2 : Initialiser le site WP2

Créer un nouveau dossier pour le site Wordpress 2
```bash
mkdir -p /root/traefix/wp2
cd /root/traefix/wp2
```

Créer un nouveau fichier docker-compose `docker-compose.yml`
```yaml
version: "3"

networks:
  web:
    external: true
  internal2:
    external: false

services:
  wp2:
    image: wordpress:4.9.8-apache
    environment:
      WORDPRESS_DB_PASSWORD:
    labels:
      - traefik.http.routers.wp2.rule=Host(`wp2.devopsviet.com`)
      - traefik.http.routers.wp2.tls=true
      - traefik.http.routers.wp2.tls.certresolver=lets-encrypt
      - traefik.port=80
    networks:
      - internal2
      - web
    depends_on:
      - mysql
  mysql:
    image: mysql:5.7
    environment:
      MYSQL_ROOT_PASSWORD:
    networks:
      - internal2
    labels:
      - traefik.enable=false

  adminerwp2:
    image: adminer:4.6.3-standalone
    labels:
      - traefik.http.routers.adminerwp2.rule=Host(`db-admin-wp2.devopsviet.com`)
      - traefik.http.routers.adminerwp2.tls=true
      - traefik.http.routers.adminerwp2.tls.certresolver=lets-encrypt
      - traefik.port=8080
    networks:
      - internal2
      - web
    depends_on:
      - mysql
```

Démarrer le site WP2
```bash
docker-compose up -d
```

Résultat :
```bash
root@devtest-traefik:~/traefix/wp2# docker-compose up -d

Creating network "wp2_internal2" with the default driver
Creating wp2_mysql_1 ... done
Creating wp2_adminerwp2_1 ... done
Creating wp2_wp2_1        ... done
```

Accéder au site wp2 et configurer Wordpress basique : https://wp2.devopsviet.com/

![](/images/setup/setup-traefik-docker/pic7.png)

Résultat

![](/images/setup/setup-traefik-docker/pic8.png)

Accéder à la page d'administration de la base de données : https://db-admin-wp2.devopsviet.com/

Note : 
- Serveur : mysql
- Nom d'utilisateur : root
- Mot de passe : cloud3652020
- Base de données : Laisser vide

![](/images/setup/setup-traefik-docker/pic9.png)

Résultat

![](/images/setup/setup-traefik-docker/pic10.png)

## Source

https://www.digitalocean.com/community/tutorials/how-to-use-traefik-v2-as-a-reverse-proxy-for-docker-containers-on-ubuntu-20-04

