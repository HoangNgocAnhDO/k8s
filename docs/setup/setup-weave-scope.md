# Guide d'installation de Weave Scope

## Vue d'ensemble

Weave Scope est un outil de visualisation et de surveillance pour les plateformes Docker et Kubernetes. Weave Scope offre une interface complète pour que les administrateurs puissent surveiller l'ensemble de l'infrastructure des conteneurs ainsi que pour le dépannage en cas de problème.

Pour en savoir plus : https://www.weave.works/docs/scope/latest/introducing/

## Préparation

Déployer un cluster de 3 nœuds selon le document :
- [Guide d'installation de Kubernetes (utilisant Kubeadmin) sur CentOS 7](/docs/setup/install-k8s-centos7-kubeadm.md)

Déployer NFS comme stockage pour le cluster K8s selon le document :
- [Guide d'installation de NFS comme stockage pour K8s](/docs/setup/install-nfs-storage-k8s.md)

## Partie 1 : Installation de Weave Scope

### Étape 1 : Télécharger le code source
```
mkdir weave-scope
cd weave-scope
curl -fsSL -o weave-scope.yml https://cloud.weave.works/k8s/scope.yaml?k8s-version=$(kubectl version | base64 | tr -d '\n')
```

Résultat
```
[root@master1181 weave-scope]# ll
total 16
-rw-r--r-- 1 root root 14824 16:39  2 Nov weave-scope.yml
```

### Étape 2 : Installer Weave Scope
```
kubectl apply -f weave-scope.yml
```

Résultat
```
[root@master1181 weave-scope]# kubectl apply -f weave-scope.yml
namespace/weave created
serviceaccount/weave-scope created
clusterrole.rbac.authorization.k8s.io/weave-scope created
clusterrolebinding.rbac.authorization.k8s.io/weave-scope created
deployment.apps/weave-scope-app created
service/weave-scope-app created
deployment.apps/weave-scope-cluster-agent created
daemonset.apps/weave-scope-agent created
```

### Étape 3 : Vérifier le service

```
[root@master1181 weave-scope]# kubectl get --namespace=weave daemonset weave-scope-agent
NAME                DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR   AGE
weave-scope-agent   3         3         3       3            3           <none>          46s

[root@master1181 weave-scope]# kubectl get --namespace=weave deployments
NAME                        READY   UP-TO-DATE   AVAILABLE   AGE
weave-scope-app             1/1     1            1           57s
weave-scope-cluster-agent   1/1     1            1           57s

[root@master1181 weave-scope]# kubectl get --namespace=weave services
NAME              TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)   AGE
weave-scope-app   ClusterIP   10.108.233.164   <none>        80/TCP    62s

[root@master1181 weave-scope]# kubectl get --namespace=weave pod | grep weave
weave-scope-agent-b2769                      1/1     Running   0          71s
weave-scope-agent-db454                      1/1     Running   0          71s
weave-scope-agent-dk974                      1/1     Running   0          71s
weave-scope-app-545ddf96b4-scbk7             1/1     Running   0          71s
weave-scope-cluster-agent-74c596c6b7-bmrn5   1/1     Running   0          71s
```

### Étape 4 : Exposer l'interface `weave-scope-app`

Note :
- Modifier le type de service de `ClusterIP` à `NodePort`
Exécution
```bash
kubectl edit --namespace=weave service weave-scope-app
```

Ajustement
```yaml
spec:
  clusterIP: 10.108.233.164
  ports:
  - name: app
    port: 80
    protocol: TCP
    targetPort: 4040
  selector:
    app: weave-scope
    name: weave-scope-app
    weave-cloud-component: scope
    weave-scope-component: app
  sessionAffinity: None
  type: NodePort # MODIFIEZ CETTE VALEUR
status:
  loadBalancer: {}
```

Résultat
```bash
[root@master1181 weave-scope]# kubectl edit --namespace=weave service weave-scope-app
service/weave-scope-app edited
```

Vérification du service
```bash
kubectl get --namespace=weave services
```

Résultat
```bash
[root@master1181 weave-scope]# kubectl get --namespace=weave services
NAME              TYPE       CLUSTER-IP       EXTERNAL-IP   PORT(S)        AGE
weave-scope-app   NodePort   10.108.233.164   <none>        80:32591/TCP   6m55s
```

Le service `weave-scope-app` a été exposé sur le port `32591`.

Ainsi, nous accéderons à l'adresse : https://<IP_K8S_CLUSTER>:32591

### Étape 5 : Accès à l'interface

Dans cet exemple, nous accéderons via l'adresse : http://10.10.11.81:32591/

Résultat

![](/images/setup/setup-weave-scope/pic1.png)

Ceci conclut le document de guide sur le déploiement de l'outil Weave Scope.

## Sources

https://www.weave.works/docs/scope/latest/installing/#k8s

