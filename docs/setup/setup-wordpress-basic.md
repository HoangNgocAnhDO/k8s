# Guide de déploiement de base de Wordpress sur K8s

## Préparation

Déployer un cluster de 3 nœuds selon le document :
- [Guide d'installation de Kubernetes (utilisant Kubeadmin) sur CentOS 7](/docs/setup/install-k8s-centos7-kubeadm.md)

Déployer NFS comme stockage pour le cluster K8s selon le document :
- [Guide d'installation de NFS comme stockage pour K8s](/docs/setup/install-nfs-storage-k8s.md)

Note :
- L'ensemble des manifests sources se trouve dans le répertoire `/src/wp`

## Partie 1 : Création d'une partition de stockage pour les données Wordpress

> À réaliser sur le serveur NFS

Note :
- Lors de l'utilisation de NFS comme stockage partagé pour K8s, les données des Pods seront montées sur la partition NFS en tant que Volume Persistant.
- Lors du déploiement de Wordpress sur K8s, nous devrons créer une nouvelle partition pour stocker les données de Wordpress sur le serveur NFS

### Étape 1 : Créer un nouveau chemin de montage NFS

```
cd /storagek8s
mkdir wpdata1
mkdir wpdata2
```

### Bước 2: Droits pour les répertoires

```
chown -R nfsnobody:nfsnobody /storagek8s/wpdata1
chown -R nfsnobody:nfsnobody /storagek8s/wpdata2
chmod -R 777 /storagek8s/wpdata1
chmod -R 777 /storagek8s/wpdata2
```

Résultat
```
[root@nfs1199 storagek8s]# chown -R nfsnobody:nfsnobody /storagek8s/wpdata1
[root@nfs1199 storagek8s]# chown -R nfsnobody:nfsnobody /storagek8s/wpdata2
[root@nfs1199 storagek8s]# chmod -R 777 /storagek8s/wpdata1
[root@nfs1199 storagek8s]# chmod -R 777 /storagek8s/wpdata2
[root@nfs1199 storagek8s]# ll
total 0
drwxrwxrwx 2 nfsnobody nfsnobody 6 13:59  2 Th11 wpdata1
drwxrwxrwx 2 nfsnobody nfsnobody 6 13:40  2 Th11 wpdata2
```

## Partie 2 : Déploiement de Wordpress

> À réaliser sur le nœud Master du cluster k8s, avec les droits `root`

### Étape 1 : Créer un nouveau répertoire pour les fichiers manifest

```bash
mkdir -p /root/wp-k8s
cd /root/wp-k8s
```

### Étape 2 : Créer un nouveau Volume Persistant

Créer un nouveau fichier PV `wordpress-pv.yml`

```

[root@master1181 wp-k8s]# cat wordpress-pv.yml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: wordpress-data-1
spec:
  capacity:
    storage: 20Gi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteOnce
  nfs:
    path: /storagek8s/wpdata1
    server: 10.10.11.99
    readOnly: false
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: wordpress-data-2
spec:
  capacity:
    storage: 20Gi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteOnce
  nfs:
    path: /storagek8s/wpdata2
    server: 10.10.11.99
    readOnly: false
```

Créer PV
```
kubectl apply -f wordpress-pv.yml
```

Résultat
```
[root@master1181 wp-k8s]# kubectl apply -f wordpress-pv.yml
persistentvolume/wordpress-data-1 created
persistentvolume/wordpress-data-2 created
```

Vérifier
```
[root@master1181 wp-k8s]# kubectl get pv
NAME               CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS      CLAIM   STORAGECLASS   REASON   AGE
wordpress-data-1   20Gi       RWO            Retain           Available                                   26s
wordpress-data-2   20Gi       RWO            Retain           Available                                   26s
```

### Etape 3: Mdp Admin pour WP

```
echo -n 'admin' | base64
```

Résultat
```
[root@master1181 wp-k8s]# echo -n 'admin' | base64
YWRtaW4=
```

Créer file `wordpress-secret.yml`

```
[root@master1181 wp-k8s]# cat wordpress-secret.yml
apiVersion: v1
kind: Secret
metadata:
  name: mysql-pass
type: Opaque
data:
  password: YWRtaW4=
```

Créer resource Secret
```
kubectl apply -f wordpress-secret.yml
```

Résultat
```
[root@master1181 wp-k8s]# kubectl get secret 
NAME                  TYPE                                  DATA   AGE
default-token-56nlp   kubernetes.io/service-account-token   3      38m
mysql-pass            Opaque                                1      13s

[root@master1181 wp-k8s]# kubectl get secret 
NAME                  TYPE                                  DATA   AGE
default-token-56nlp   kubernetes.io/service-account-token   3      38m
mysql-pass            Opaque                                1      13s
[root@master1181 wp-k8s]# kubectl describe secret mysql-pass
Name:         mysql-pass
Namespace:    default
Labels:       <none>
Annotations:  <none>

Type:  Opaque

Data
====
password:  5 bytes
```

### Etape 4: Télécharger le fichier manifest

Remarque :
- Télécharger 2 nouveaux fichiers manifest `mysql-deployment.yaml` et `wordpress-deployment.yaml`
- `mysql-deployment.yaml` : Base de données pour WP
- `wordpress-deployment.yaml` : Code source de WP

Télécharger les fichiers manifest
```bash
curl -LO https://k8s.io/examples/application/wordpress/mysql-deployment.yaml
curl -LO https://k8s.io/examples/application/wordpress/wordpress-deployment.yaml
```

### Étape 5 : Déployer la base de données WP
```bash
kubectl apply -f mysql-deployment.yaml
```

Résultat
```bash
[root@master1181 wp-k8s]# kubectl apply -f mysql-deployment.yaml
service/wordpress-mysql créé
persistentvolumeclaim/mysql-pv-claim créé
deployment.apps/wordpress-mysql créé
```

Vérification
```bash
# Vérifier le déploiement
[root@master1181 wp-k8s]# kubectl get deployments
NAME              READY   UP-TO-DATE   AVAILABLE   AGE
wordpress-mysql   1/1     1            1           2m

# Vérifier le pod
[root@master1181 wp-k8s]# kubectl get pods
NAME                              READY   STATUS    RESTARTS   AGE
wordpress-mysql-6c479567b-kx5x5   1/1     Running   0          2m29s

# Vérifier le pvc
[root@master1181 wp-k8s]# kubectl get pvc
NAME             STATUS   VOLUME             CAPACITY   ACCESS MODES   STORAGECLASS   AGE
mysql-pv-claim   Bound    wordpress-data-1   20Gi       RWO                           2m47s

# Vérifier les services
[root@master1181 wp-k8s]# kubectl get services
NAME              TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)    AGE
kubernetes        ClusterIP   10.96.0.1    <none>        443/TCP    43m
wordpress-mysql   ClusterIP   None         <none>        3306/TCP   3m49s
```

Vérifier la base de données
```bash
kubectl run -it --rm --image=mysql:5.6 --restart=Never mysql-client -- mysql -h wordpress-mysql -padmin
```

Résultat
```bash
[root@master1181 wp-k8s]# kubectl run -it --rm --image=mysql:5.6 --restart=Never mysql-client -- mysql -h wordpress-mysql -padmin
Si vous ne voyez pas d'invite de commande, essayez d'appuyer sur entrée.

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
+--------------------+
3 rows in set (0.01 sec)

mysql> Exit
```
### Etape 5: Exécuter source code WP
```
kubectl apply -f wordpress-deployment.yaml
```

Résultat
```
[root@master1181 wp-k8s]# kubectl apply -f wordpress-deployment.yaml
service/wordpress created
persistentvolumeclaim/wp-pv-claim created
deployment.apps/wordpress created
```

Vérifier
```
# Vérifier deployment
[root@master1181 wp-k8s]# kubectl get deployments
NAME              READY   UP-TO-DATE   AVAILABLE   AGE
wordpress         1/1     1            1           42s
wordpress-mysql   1/1     1            1           5m23s

# Vérifier pod
[root@master1181 wp-k8s]# kubectl get pods
NAME                              READY   STATUS    RESTARTS   AGE
wordpress-5994d99f46-55xxd        1/1     Running   0          46s
wordpress-mysql-6c479567b-kx5x5   1/1     Running   0          5m27s

# Vérifier pvc
[root@master1181 wp-k8s]# kubectl get pvc
NAME             STATUS   VOLUME             CAPACITY   ACCESS MODES   STORAGECLASS   AGE
mysql-pv-claim   Bound    wordpress-data-1   20Gi       RWO                           5m30s
wp-pv-claim      Bound    wordpress-data-2   20Gi       RWO                           49s

# Vérifier services
[root@master1181 wp-k8s]# kubectl get services
NAME              TYPE           CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE
kubernetes        ClusterIP      10.96.0.1       <none>        443/TCP        44m
wordpress         LoadBalancer   10.102.139.84   <pending>     80:30837/TCP   52s
wordpress-mysql   ClusterIP      None            <none>        3306/TCP       5m33s
```

### Étape 6 : Modifier le Type du service WordPress en NodePort

Remarque :
- Modifier le type de service de `LoadBalancer` à `NodePort`
- Effectuer les modifications du fichier et enregistrer avec la syntaxe de l'éditeur `vim`

```bash
kubectl edit services wordpress
```

Résultat
```yaml
# Veuillez modifier l'objet ci-dessous. Les lignes commençant par un '#' seront ignorées,
# et un fichier vide annulera la modification. Si une erreur se produit lors de l'enregistrement, ce fichier sera
# rouvert avec les échecs pertinents.
#

apiVersion: v1
kind: Service
metadata:
  annotations:
    kubectl.kubernetes.io/last-applied-configuration: |
      {"apiVersion":"v1","kind":"Service","metadata":{"annotations":{},"labels":{"app":"wordpress"},"name":"wordpress","namespace":"default"},"spec":{"ports":[{"port":80}],"selector":{"app":"wordpress","tier":"frontend"},"type":"LoadBalancer"}}
  creationTimestamp: "2020-11-02T02:34:09Z"
  labels:
    app: wordpress
  name: wordpress
  namespace: default
  resourceVersion: "6761"
  selfLink: /api/v1/namespaces/default/services/wordpress
  uid: 4cdcfca7-b4c8-48ae-b866-a53ef09359c6
spec:
  clusterIP: 10.102.139.84
  externalTrafficPolicy: Cluster
  ports:
  - nodePort: 30837
    port: 80
    protocol: TCP
    targetPort: 80
  selector:
    app: wordpress
    tier: frontend
  sessionAffinity: None
  type: NodePort # CHỈNH GIÁ TRỊ NÀY THÀNH NodePort
status:
  loadBalancer: {}
```

Résultat
```
[root@master1181 ~]# kubectl edit services wordpress
service/wordpress edited
```

Vérifier
```
[root@master1181 wp-k8s]# kubectl get services
NAME              TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)        AGE
kubernetes        ClusterIP   10.96.0.1      <none>        443/TCP        91m
wordpress         NodePort    10.97.76.197   <none>        80:32457/TCP   56s
wordpress-mysql   ClusterIP   None           <none>        3306/TCP       2m43s
```
## Remarques
- Le port exposé du service WP est `30837`.
- Nous pouvons accéder au service WP depuis tous les nœuds Worker du cluster
- Par exemple, dans cet article, les adresses sont `http://10.10.11.81:32457, http://10.10.11.82:32457, http://10.10.11.83:32457`

### Étape 7 : Configuration de WP

Accéder via le navigateur : http://10.10.11.81:32457

![](/images/setup/setup-wordpress-basic/pic1.png)

![](/images/setup/setup-wordpress-basic/pic2.png)

![](/images/setup/setup-wordpress-basic/pic3.png)

![](/images/setup/setup-wordpress-basic/pic4.png)

![](/images/setup/setup-wordpress-basic/pic5.png)

![](/images/setup/setup-wordpress-basic/pic6.png)

À ce stade, la configuration de base de WP sur K8s est terminée.

## Partie 3 : Nettoyage

Cette étape consiste à supprimer toutes les ressources créées sur le cluster K8s

```bash
kubectl delete -f wordpress-deployment.yaml
kubectl delete -f mysql-deployment.yaml
kubectl delete -f wordpress-pv.yml
kubectl delete -f wordpress-secret.yml
```

Résultats :
```bash
[root@master1181 wp-k8s]# kubectl delete -f wordpress-deployment.yaml
service "wordpress" deleted
persistentvolumeclaim "wp-pv-claim" deleted
deployment.apps "wordpress" deleted

[root@master1181 wp-k8s]# kubectl delete -f mysql-deployment.yaml
service "wordpress-mysql" deleted
persistentvolumeclaim "mysql-pv-claim" deleted
deployment.apps "wordpress-mysql" deleted

[root@master1181 wp-k8s]# kubectl delete -f wordpress-pv.yml
persistentvolume "wordpress-data-1" deleted
persistentvolume "wordpress-data-2" deleted

[root@master1181 wp-k8s]# kubectl delete -f wordpress-secret.yml
secret "mysql-pass" deleted
```

## Sources

https://kubernetes.io/docs/tutorials/stateful-application/mysql-wordpress-persistent-volume/

